﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmProductos : Form
    {
        private readonly ProductosServicio _productosServicio;
        public FrmProductos()
        {
            InitializeComponent();
            _productosServicio = new ProductosServicio();
            lblUsuarioLogueado.Text = UsuarioLogueado.NombreUsuario;
            IniciarDataGridView();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else 
            {
                Application.Exit();
            }
        }



        private void IniciarDataGridView()
        {
            // Cree un DataGridView no vinculado declarando un recuento de columnas.
            dgvProductos.ColumnCount = 1;
            dgvProductos.ColumnHeadersVisible = true;

            // Configuramos la AutoGenerateColumns en false para que no se autogeneren las columnas
            dgvProductos.AutoGenerateColumns = false;

            // Cambia el estilo de la cabecera de la grilla.
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();

            columnHeaderStyle.BackColor = Color.Beige;
            columnHeaderStyle.Font = new Font("Verdana", 8, FontStyle.Bold);
            dgvProductos.ColumnHeadersDefaultCellStyle = columnHeaderStyle;


            // Definimos el ancho de la columna.
            dgvProductos.Columns[0].Name = "Nombre de producto";
            dgvProductos.Columns[0].DataPropertyName = "NombreProducto";

            dgvProductos.Enabled = false;

            // Cambia el tamaño de la altura de los encabezados de columna.
            dgvProductos.AutoResizeColumnHeadersHeight();

            // Cambia el tamaño de todas las alturas de fila para ajustar el contenido de todas las celdas que no sean de encabezado.
            dgvProductos.AutoResizeRows(
                DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);


            btnConsultar.Enabled = false;
            SetearContadorRegistros(0);

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            dgvProductos.Enabled = true;
            if (!chkDadosBaja.Checked && !chkObtenerTodos.Checked)
            {
                var nombre = txtProductos.Text;
                if (nombre != null)
                {
                    
                    var productos = _productosServicio.BuscarPorNombre(nombre);

                    dgvProductos.DataSource = productos;
                    SetearContadorRegistros(dgvProductos.Rows.Count);
                    
                }
            }
            else if (chkDadosBaja.Checked)
            {
                dgvProductos.DataSource = _productosServicio.ObtenerDadosDeBaja();
                SetearContadorRegistros(dgvProductos.Rows.Count);

                foreach (DataGridViewRow dgvr in dgvProductos.Rows) 
                {
                    
                    var producto = (Producto)dgvProductos.CurrentRow.DataBoundItem;
                    if (producto.Borrado == true) 
                    { 
                        dgvr.DefaultCellStyle.ForeColor= Color.Red;
                    }
                    else
                    {
                        dgvr.DefaultCellStyle.ForeColor = Color.Black;
                    }
                }

            }
            else
            {
                dgvProductos.DataSource = _productosServicio.ObtenerProductos();
                SetearContadorRegistros(dgvProductos.Rows.Count);
            }

        }


        private void dgvProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnActualizar.Enabled = true;
            btnEliminar.Enabled = true;
        }

        private void chkTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDadosBaja.Checked)
            {
                txtProductos.Enabled = false;
                txtProductos.Text = null;
                chkObtenerTodos.Enabled = false;
                btnConsultar.Enabled = true;

            }
            else
            {
                txtProductos.Enabled = true;
                chkObtenerTodos.Enabled = true;
                btnConsultar.Enabled = false;

            }
        }


        private void chkSinBorrar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkObtenerTodos.Checked)
            {
                txtProductos.Enabled = false;
                txtProductos.Text = null;
                chkDadosBaja.Enabled = false;
                btnConsultar.Enabled = true;

            }
            else
            {
                txtProductos.Enabled = true;
                chkDadosBaja.Enabled = true;
                btnConsultar.Enabled = false;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            dgvProductos.DataSource = null;
            dgvProductos.Enabled = false;
            chkObtenerTodos.Enabled = true;
            chkDadosBaja.Checked = false;
            chkObtenerTodos.Checked = false;
            chkDadosBaja.Enabled = true;
            txtProductos.Enabled = true;
            txtProductos.Text = null;
            SetearContadorRegistros(0);

        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }

        private void txtProductos_TextChanged(object sender, EventArgs e)
        {
            if(txtProductos.Text.Length != 0)
            {
                btnConsultar.Enabled = true;

            }
            else if(txtProductos.Text.Length == 0)
            {
                btnConsultar.Enabled = false;

            }
        }

        private void SetearContadorRegistros(int total)
        {
            lblContadorRegistros.Text = null;
            lblContadorRegistros.Text = $"Total de productos: {total}";
        }

        private void btnProyectos_Click(object sender, EventArgs e)
        {
            var frmProyectos = new FrmProyecto();
            frmProyectos.Show();
            this.Close();
        }

        private void btnPlanesdePrueba_Click(object sender, EventArgs e)
        {
            var frmPPrueba = new FrmPdePruebas();
            frmPPrueba.Show();
            this.Close();
        }

        private void btnCasosdePrueba_Click(object sender, EventArgs e)
        {
            var frmCasosPrueba = new FrmCdePruebas();
            frmCasosPrueba.Show();
            this.Close();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            var frmReportes = new FrmReportes();
            frmReportes.Show();
            this.Close();
        }
        private void btnAgregar_Click_1(object sender, EventArgs e)
        {
            var formulario = new FrmABMProductos();
            formulario.ShowDialog();
            btnConsultar_Click(sender, e);
        }

        private void btnActualizar_Click_1(object sender, EventArgs e)
        {
            var formulario = new FrmABMProductos();
            if (dgvProductos.CurrentRow != null)
            {
                var producto = (Producto)dgvProductos.CurrentRow.DataBoundItem;
                if (producto != null)
                {
                    if (producto.Borrado == true)
                    {
                        MessageBox.Show("No se puede editar un producto borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        formulario.InicializarFormulario(FormMode.Actualizar, producto);
                        formulario.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            var formulario = new FrmABMProductos();
            if (dgvProductos.CurrentRow != null)
            {
                var producto = (Producto)dgvProductos.CurrentRow.DataBoundItem;
                if (producto != null)
                {
                    if (producto.Borrado == true)
                    {
                        MessageBox.Show("No se puede eliminar un producto ya borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        formulario.InicializarFormulario(FormMode.Eliminar, producto);
                        formulario.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btnCiclosPrueba_Click(object sender, EventArgs e)
        {
            var frmCiclosPrueba = new FrmCicloDePrueba();
            frmCiclosPrueba.Show();
            this.Close();
        }

        private void btnAgregar_MouseHover_1(object sender, EventArgs e)
        {
            var tooltipAgregar = new ToolTip();
            tooltipAgregar.SetToolTip(btnAgregar, "Agregar");
        }

        private void btnActualizar_MouseHover_1(object sender, EventArgs e)
        {
            var tooltipActualizar = new ToolTip();
            tooltipActualizar.SetToolTip(btnActualizar, "Actualizar");
        }

        private void btnEliminar_MouseHover_1(object sender, EventArgs e)
        {
            var tooltipEliminar = new ToolTip();
            tooltipEliminar.SetToolTip(btnEliminar, "Eliminar");
        }

        private void btnVolver_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
            var formPrincipal = new FrmPrincipal();
            formPrincipal.Show();
            return;
        }

        private void btnSalirYDesc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea cerrar sesion?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                this.Close();
                var login = new FrmLogin();
                login.ShowDialog();
                return;
            }
        }
    }

    
}
