﻿using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
            lblUsuarioLogueado.Text = UsuarioLogueado.NombreUsuario;
        }

        private void btncerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            var frmProductos = new FrmProductos();
            frmProductos.Show();
            this.Close();
        }

        private void btnPlanesdePrueba_Click(object sender, EventArgs e)
        {
            var frmPlanesPrueba = new FrmPdePruebas();
            frmPlanesPrueba.Show();
            this.Close();
        }

        private void btnProyectos_Click(object sender, EventArgs e)
        {

            var frmProyecto = new FrmProyecto();
            frmProyecto.Show();
            this.Close();
        }

        private void btnCasosdePrueba_Click(object sender, EventArgs e)
        {
            var frmCasosPrueba = new FrmCdePruebas();
            frmCasosPrueba.Show();
            this.Close();
        }

        private void btnCiclosPrueba_Click(object sender, EventArgs e)
        {
            var frmCiclosPrueba = new FrmCicloDePrueba();
            frmCiclosPrueba.Show();
            this.Close();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            var frmReportes = new FrmReportes();
            frmReportes.Show();
            this.Close();
        }

        private void btnSalirYDesc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                this.Close();
                var login = new FrmLogin();
                login.ShowDialog();
                return;
            }
        }
    }
}
