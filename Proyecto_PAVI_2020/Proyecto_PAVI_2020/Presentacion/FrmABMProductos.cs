﻿using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmABMProductos : Form
    {
        private FormMode formMode = FormMode.Nuevo;
        private readonly ProductosServicio _productosServicio;
        private Producto _productoSeleccionado;

        public FrmABMProductos()
        {
            InitializeComponent();
            _productosServicio = new ProductosServicio();
        }



        private void frmABMProductos_Cargar(object sender, System.EventArgs eve)
        {
            switch (formMode)
            {
                case FormMode.Nuevo:
                    {
                        txtABMProducto.Text = "Agregar producto";
                        break;
                    }
                case FormMode.Actualizar:
                    {
                        txtABMProducto.Text = "Actualizar producto";
                        MostrarDatos();
                        txtNombre.Enabled = true;

                        break;
                    }

                case FormMode.Eliminar:
                    {
                        MostrarDatos();
                        txtABMProducto.Text = "Eliminar producto";
                        txtNombre.Enabled = false;
                        break;
                    }
            }

        }
        private void MostrarDatos()
        {
            if (_productoSeleccionado != null)
            {
                txtNombre.Text = _productoSeleccionado.NombreProducto;
            }
        }
        public void InicializarFormulario(FormMode op, Producto productoSelected)
        {
            formMode = op;
            _productoSeleccionado = productoSelected;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            switch (formMode)
            {
                case FormMode.Nuevo:
                    if (txtNombre.Text != "")
                    {
                        
                        var existeProducto = _productosServicio.ExisteProductoEnDb(txtNombre.Text);
                        if(existeProducto == false)
                        {
                            var productoNuevo = new Producto();
                            productoNuevo.NombreProducto = txtNombre.Text;
                            var agregado = _productosServicio.AgregarProductos(productoNuevo);
                            if (agregado == true)
                            {
                                MessageBox.Show("Se ha agregado correctamente el producto.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Dispose();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo agregar el producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No se puede agregar un producto existente.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show("Ingrese un nombre de producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Actualizar:
                    if (txtNombre.Text != "" && txtNombre.Text != _productoSeleccionado.NombreProducto)
                    {
                        var existeProducto = _productosServicio.ExisteProductoEnDb(txtNombre.Text);
                        if(existeProducto == false)
                        {
                            var productoEntidad = new Producto();
                            productoEntidad.IdProducto = _productoSeleccionado.IdProducto;
                            productoEntidad.NombreProducto = txtNombre.Text;
                            var modificado = _productosServicio.ActualizarProductos(productoEntidad);
                            if (modificado == true)
                            {
                                MessageBox.Show("Se ha actualizado correctamente el producto.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Dispose();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo actualizar el producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                this.Dispose();
                            }
                        }
                        else
                        {
                            MessageBox.Show("El producto ya existe.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else if (txtNombre.Text == "")
                    {
                        MessageBox.Show("Debe ingresar un nombre de producto nuevo para actualizarlo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (_productoSeleccionado.NombreProducto == txtNombre.Text)
                    {
                        MessageBox.Show("Ingrese un nombre distinto para actualizar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Eliminar:
                    var productoAEliminar = new Producto();
                    productoAEliminar.IdProducto = _productoSeleccionado.IdProducto;
                    productoAEliminar.NombreProducto = _productoSeleccionado.NombreProducto;
                    var eliminado = _productosServicio.EliminarProductos(_productoSeleccionado);
                    if (eliminado == true)
                    {
                        MessageBox.Show("Se ha eliminado correctamente el producto.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Dispose();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Dispose();
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        private void btnAceptar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.btnAceptar_Click(sender, e);
                
        }

        private void btnCancelar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.Close();
        }
    }
}

