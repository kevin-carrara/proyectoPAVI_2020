﻿using Microsoft.VisualBasic.Devices;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmPdePruebas : Form
    {
        private readonly PlanesDePruebaServicio _planesDePruebaServicio;
        private readonly UsuarioServicio _usuarioServicio;
        private readonly ProyectoServicio _proyectoServicio;
        public FrmPdePruebas()
        {
            InitializeComponent();
            lblUsuarioLogueado.Text = UsuarioLogueado.NombreUsuario;
            _planesDePruebaServicio = new PlanesDePruebaServicio();
            _usuarioServicio = new UsuarioServicio();
            _proyectoServicio = new ProyectoServicio();

        }

        private void btnProyectos_Click(object sender, EventArgs e)
        {
            var frmProyectos = new FrmProyecto();
            frmProyectos.Show();
            this.Close();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            var frmProductos = new FrmProductos();
            frmProductos.Show();
            this.Close();
        }

        private void btnCasosdePrueba_Click(object sender, EventArgs e)
        {
            var frmCPrueba = new FrmCdePruebas();
            frmCPrueba.Show();
            this.Close();
        }


        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }

        private void IniciarDataGridView()
        {
            // Cree un DataGridView no vinculado declarando un recuento de columnas.
            dgvPDePrueba.ColumnCount = 4;
            dgvPDePrueba.ColumnHeadersVisible = true;

            // Configuramos la AutoGenerateColumns en false para que no se autogeneren las columnas
            dgvPDePrueba.AutoGenerateColumns = false;

            // Cambia el estilo de la cabecera de la grilla.
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();

            columnHeaderStyle.BackColor = Color.Beige;
            columnHeaderStyle.Font = new Font("Verdana", 8, FontStyle.Bold);
            dgvPDePrueba.ColumnHeadersDefaultCellStyle = columnHeaderStyle;


            // Definimos el ancho de la columna.
            dgvPDePrueba.Columns[0].Name = "Nombre";
            dgvPDePrueba.Columns[0].DataPropertyName = "Nombre";

            dgvPDePrueba.Columns[1].Name = "Descripcion";
            dgvPDePrueba.Columns[1].DataPropertyName = "Descripcion";

            dgvPDePrueba.Columns[2].Name = "Responsable";
            dgvPDePrueba.Columns[2].DataPropertyName = "NombreResponsable";

            dgvPDePrueba.Columns[3].Name = "Proyecto";
            dgvPDePrueba.Columns[3].DataPropertyName = "DescripcionProyecto";

            dgvPDePrueba.Enabled = false;
            

            // Cambia el tamaño de la altura de los encabezados de columna.
            dgvPDePrueba.AutoResizeColumnHeadersHeight();

            // Cambia el tamaño de todas las alturas de fila para ajustar el contenido de todas las celdas que no sean de encabezado.
            dgvPDePrueba.AutoResizeRows(
                DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);

            SetearContadorRegistros(0);

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            dgvPDePrueba.Enabled = true;

            if (!chkDadosBaja.Checked && !chkObtenerTodos.Checked)
            {
             
                var planes = _planesDePruebaServicio.BuscarConFiltros(txtPlanesPrueba.Text,(int?)cmbResponsable.SelectedValue, (int?)cmbProyecto.SelectedValue);
                dgvPDePrueba.DataSource = planes;
                SetearContadorRegistros(dgvPDePrueba.Rows.Count);
            }
            else if (chkDadosBaja.Checked)
            {
                dgvPDePrueba.DataSource = _planesDePruebaServicio.ObtenerDadosDeBaja();
                SetearContadorRegistros(dgvPDePrueba.Rows.Count);

                foreach (DataGridViewRow dgvr in dgvPDePrueba.Rows)
                {

                    var planDePrueba = (PlanesDePruebas)dgvPDePrueba.CurrentRow.DataBoundItem;
                    if (planDePrueba.Borrado == true)
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.Red;
                    }
                    else
                    {
                        dgvr.DefaultCellStyle.ForeColor = Color.Black;
                    }
                }

            }
            else if (chkObtenerTodos.Checked)
            {
                dgvPDePrueba.DataSource = _planesDePruebaServicio.ObtenerTodos();
                SetearContadorRegistros(dgvPDePrueba.Rows.Count);
            }
            
          
           
        }

        private void SetearContadorRegistros(int total)
        {
            lblContadorRegistros.Text = null;
            lblContadorRegistros.Text = $"Total de planes de prueba: {total}";
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            dgvPDePrueba.DataSource = null;
            dgvPDePrueba.Enabled = false;
            cmbProyecto.SelectedIndex = -1;
            cmbResponsable.SelectedIndex = -1;
            chkObtenerTodos.Enabled = true;
            chkDadosBaja.Checked = false;
            chkObtenerTodos.Checked = false;
            chkDadosBaja.Enabled = true;
            txtPlanesPrueba.Enabled = true;
            txtPlanesPrueba.Text = null;
            SetearContadorRegistros(0);
        }

        private void dgvPDePrueba_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnActualizar.Enabled = true;
            btnEliminar.Enabled = true;
        }

        private void chkDadosBaja_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDadosBaja.Checked)
            {
                txtPlanesPrueba.Enabled = false;
                txtPlanesPrueba.Text = null;
                chkObtenerTodos.Enabled = false;
                cmbProyecto.SelectedIndex = -1;
                cmbProyecto.Enabled = false;
                cmbResponsable.SelectedIndex = -1;
                cmbResponsable.Enabled = false;

            }
            else
            {
                txtPlanesPrueba.Enabled = true;
                chkObtenerTodos.Enabled = true;
                cmbResponsable.Enabled = true;
                cmbProyecto.Enabled = true;

            }
        }

        private void chkObtenerTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (chkObtenerTodos.Checked)
            {
                txtPlanesPrueba.Enabled = false;
                txtPlanesPrueba.Text = null;
                chkDadosBaja.Enabled = false;
                btnConsultar.Enabled = true;
                cmbProyecto.SelectedIndex = -1;
                cmbProyecto.Enabled = false;
                cmbResponsable.SelectedIndex = -1;
                cmbResponsable.Enabled = false;

            }
            else
            {
                txtPlanesPrueba.Enabled = true;
                chkDadosBaja.Enabled = true;
                cmbProyecto.Enabled = true;
                cmbResponsable.Enabled = true;
            }
        }

        private void LlenarCombo(ComboBox cbo, Object source, string display, String value)
        {
            cbo.DataSource = source;
            cbo.DisplayMember = display;
            cbo.ValueMember = value;
            cbo.SelectedIndex = -1;
        }

        private void FrmPdePruebas_Load(object sender, EventArgs e)
        {
            IniciarDataGridView();
            var usuarios = _usuarioServicio.ObtenerUsuarios();
            var proyectos = _proyectoServicio.ObtenerProyectos();
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
            LlenarCombo(cmbProyecto, proyectos, "Descripcion", "IdProyecto");

        }

        private void btnAgregar_Click_1(object sender, EventArgs e)
        {
            var formulario = new FrmABMPdePrueba();
            formulario.ShowDialog();
            btnConsultar_Click(sender, e);
        }

        private void btnActualizar_Click_1(object sender, EventArgs e)
        {
            var abmcPPrueba = new FrmABMPdePrueba();
            if (dgvPDePrueba.CurrentRow != null)
            {
                var planPrueba = (PlanesDePruebas)dgvPDePrueba.CurrentRow.DataBoundItem;
                if (planPrueba != null)
                {
                    if (planPrueba.Borrado == true)
                    {
                        MessageBox.Show("No se puede editar un plan de prueba ya borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        abmcPPrueba.InicializarFormulario(FormMode.Actualizar, planPrueba);
                        abmcPPrueba.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            var abmcPPrueba = new FrmABMPdePrueba();
            if (dgvPDePrueba.CurrentRow != null)
            {
                var planPrueba = (PlanesDePruebas)dgvPDePrueba.CurrentRow.DataBoundItem;
                if (planPrueba != null)
                {
                    if (planPrueba.Borrado == true)
                    {
                        MessageBox.Show("No se puede eliminar un plan de prueba ya borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        abmcPPrueba.InicializarFormulario(FormMode.Eliminar, planPrueba);
                        abmcPPrueba.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnAgregar_MouseHover_1(object sender, EventArgs e)
        {
            var toolTipAgregar = new ToolTip();
            toolTipAgregar.SetToolTip(btnAgregar, "Agregar");
        }

        private void btnActualizar_MouseHover_1(object sender, EventArgs e)
        {
            var tooltipActualizar = new ToolTip();
            tooltipActualizar.SetToolTip(btnActualizar, "Actualizar");
        }

        private void btnEliminar_MouseHover_1(object sender, EventArgs e)
        {
            var tooltipEliminar = new ToolTip();
            tooltipEliminar.SetToolTip(btnEliminar, "Eliminar");
        }

        private void btnCiclosPrueba_Click(object sender, EventArgs e)
        {
            var frmCiclosPrueba = new FrmCicloDePrueba();
            frmCiclosPrueba.Show();
            this.Close();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            var frmReportes = new FrmReportes();
            frmReportes.Show();
            this.Close();
        }

        private void btnVolver_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var frmPrincipal = new FrmPrincipal();
            frmPrincipal.Show();
            this.Close();
        }

        private void btnSalirYDesc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea cerrar sesion?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                this.Close();
                var login = new FrmLogin();
                login.ShowDialog();
                return;
            }
        }

        private void btnAgregarCiclo_Click(object sender, EventArgs e)
        {
            if (dgvPDePrueba.CurrentRow != null)
            {
                var planPrueba = (PlanesDePruebas)dgvPDePrueba.CurrentRow.DataBoundItem;
                var frmCiclosPrueba = new FrmCicloDePrueba();
                frmCiclosPrueba.InicializarFormulario(planPrueba);
                frmCiclosPrueba.Show();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un plan de prueba antes de agregar un ciclo de prueba.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
                
            
        }
    }
}
