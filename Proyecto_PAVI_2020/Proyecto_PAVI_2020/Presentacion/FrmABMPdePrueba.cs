﻿using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmABMPdePrueba : Form
    {
        private readonly PlanesDePruebaServicio _planesDePruebaServicio;
        private readonly UsuarioServicio _usuarioServicio;
        private readonly ProyectoServicio _proyectoServicio;
        private FormMode formMode = FormMode.Nuevo;
        private PlanesDePruebas _planDePruebaSeleccionado;
        public FrmABMPdePrueba()
        {
            InitializeComponent();
            _usuarioServicio = new UsuarioServicio();
            _proyectoServicio = new ProyectoServicio();
            _planesDePruebaServicio = new PlanesDePruebaServicio();

        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        private void LlenarCombo(ComboBox cbo, Object source, string display, String value)
        {
            cbo.DataSource = source;
            cbo.DisplayMember = display;
            cbo.ValueMember = value;
            cbo.SelectedIndex = -1;
        }
        private void FrmABMPdePrueba_Load(object sender, EventArgs e)
        {
            
            var usuarios = _usuarioServicio.ObtenerUsuarios();
            var proyectos = _proyectoServicio.ObtenerProyectos();
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
            LlenarCombo(cmbProyecto, proyectos, "Descripcion", "Descripcion");
            switch (formMode)
            {
                case FormMode.Nuevo:
                    {
                        txtABMProducto.Text = "Agregar plan de prueba";
                        break;
                    }
                case FormMode.Actualizar:
                    {
                        txtABMProducto.Text = "Actualizar plan de prueba";
                        MostrarDatos();
                        txtNombre.Enabled = true;
                        txtDescripcion.Enabled = true;
                        cmbProyecto.Enabled = true;
                        cmbResponsable.Enabled = true;
                        break;
                    }

                case FormMode.Eliminar:
                    {
                        MostrarDatos();
                        txtABMProducto.Text = "Eliminar plan de prueba";
                        txtNombre.Enabled = false;
                        txtDescripcion.Enabled = false;
                        cmbProyecto.Enabled = false;
                        cmbResponsable.Enabled = false;
                        break;
                    }
            }
        }

        private void MostrarDatos()
        {
            if(_planDePruebaSeleccionado != null)
            {
                var proyecto = _proyectoServicio.BuscarPorId(_planDePruebaSeleccionado.IdProyecto);
                txtNombre.Text = _planDePruebaSeleccionado.Nombre;
                txtDescripcion.Text = _planDePruebaSeleccionado.Descripcion;
                if (!string.IsNullOrEmpty(proyecto.Descripcion))
                    cmbProyecto.SelectedValue = proyecto.Descripcion;
                else
                    cmbProyecto.SelectedIndex = -1;

                if (_planDePruebaSeleccionado.IdResponsable != null)
                    cmbResponsable.SelectedValue = _planDePruebaSeleccionado.IdResponsable;
                else
                    cmbResponsable.SelectedIndex = -1;
                
            }
        }

        public void InicializarFormulario(FormMode op, PlanesDePruebas planPruebaSelected)
        {
            formMode = op;
            _planDePruebaSeleccionado = planPruebaSelected;
        }



        private void btnAceptar_Click(object sender, EventArgs e)
        {
            switch (formMode)
            {
                case FormMode.Nuevo:
                    if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtDescripcion.Text) && cmbProyecto.SelectedIndex != -1)
                    {
                        var planPrueba = SetearPlanDePrueba(null, txtNombre.Text, txtDescripcion.Text, ((Proyecto)cmbProyecto.SelectedItem).IdProyecto, (int?)cmbResponsable.SelectedValue);
                        if (ExistePlanEnDb(planPrueba) == false)
                        {
                            
                            var agregado = _planesDePruebaServicio.AgregarPlanDePrueba(planPrueba);
                            if (agregado == true)
                            {
                                MessageBox.Show("Se ha agregado correctamente el plan de prueba.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo agregar el plan de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No se puede agregar un plan de prueba existente.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Todos los campos son requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Actualizar:
                    if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtDescripcion.Text) && cmbProyecto.SelectedIndex != -1)
                    {
                        var planPrueba = SetearPlanDePrueba(_planDePruebaSeleccionado.IdPlanDePrueba, txtNombre.Text, txtDescripcion.Text, ((Proyecto)cmbProyecto.SelectedItem).IdProyecto, (int?)cmbResponsable.SelectedValue);
                        if (ExistePlanEnDb(planPrueba) == false)
                        {
                            var modificado = _planesDePruebaServicio.ActualizarPlanDePrueba(planPrueba);
                            if (modificado == true)
                            {
                                MessageBox.Show("Se ha actualizado correctamente el plan de prueba.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo actualizar el plan de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                this.Close();
                            }
                        }
                        else
                        {
                            MessageBox.Show("El plan de prueba ya existe.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Todos los campos son requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Eliminar:
                    var eliminado = _planesDePruebaServicio.EliminarPlanDePrueba(_planDePruebaSeleccionado.IdPlanDePrueba);
                    if (eliminado == true)
                    {
                        MessageBox.Show("Se ha eliminado correctamente el plan de prueba.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el plan de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        { 
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool ExistePlanEnDb(PlanesDePruebas planPrueba)
        {
            var existe = _planesDePruebaServicio.ExistePPruebaEnDb(planPrueba);
            return existe;
        }

        private void btnAceptar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.btnAceptar_Click(sender, e);
        }

        private void btnCancelar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.Close();
        }

        private PlanesDePruebas SetearPlanDePrueba(int? idPlan,string nombre, string descripcion, int idProyecto, int? idResponsable)
        {
            var planPrueba = new PlanesDePruebas();
            planPrueba.IdPlanDePrueba = idPlan ?? 0;
            planPrueba.Nombre = nombre;
            planPrueba.Descripcion = descripcion;
            planPrueba.IdProyecto = idProyecto;
            planPrueba.IdResponsable = idResponsable ?? null;
            planPrueba.Borrado = false;
            return planPrueba;
        }
    }
}
