﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmProyecto : Form
    {
        private readonly ProyectoServicio _proyectoServicio;
        private readonly UsuarioServicio _usuarioServicio;
        private readonly ProductosServicio _productoServicio;

        public FrmProyecto()
        {
            InitializeComponent();
            lblUsuarioLogueado.Text = UsuarioLogueado.NombreUsuario;
            _proyectoServicio = new ProyectoServicio();
            _usuarioServicio = new UsuarioServicio();
            _productoServicio = new ProductosServicio();
            
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            dgvProyectos.Enabled = true;
            if (!chkDadosBaja.Checked && !chkObtenerTodos.Checked)
            {

                var proyecto = _proyectoServicio.BuscarPorFiltro(txtProyectos.Text, (int?)cmbResponsable.SelectedValue, (int?)cmbProducto.SelectedValue);
                dgvProyectos.DataSource = proyecto;
                SetearContadorRegistros(dgvProyectos.Rows.Count);
                
            }
            else if (chkDadosBaja.Checked)
            {
                dgvProyectos.DataSource = _proyectoServicio.ObtenerDadosDeBaja();
                SetearContadorRegistros(dgvProyectos.Rows.Count);

                foreach (DataGridViewRow dgvr in dgvProyectos.Rows)
                {

                    var proyecto = (Proyecto)dgvProyectos.CurrentRow.DataBoundItem;
                    if (proyecto.Borrado == true)
                    {
                        dgvr.DefaultCellStyle.ForeColor = Color.Red;
                    }
                    else
                    {
                        dgvr.DefaultCellStyle.ForeColor = Color.Black;
                    }
                }

            }
            else
            {
                dgvProyectos.DataSource = _proyectoServicio.ObtenerProyectos();
                SetearContadorRegistros(dgvProyectos.Rows.Count);
            }
        }
        private void SetearContadorRegistros(int total)
        {
            lblContadorRegistros.Text = null;
            lblContadorRegistros.Text = $"Total de proyectos: {total}";
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            this.Close();
            var frmProducto = new FrmProductos();
            frmProducto.Show();
            return;
            
        }

        private void btnPdePruebas_Click(object sender, EventArgs e)
        {
            this.Close();
            var frmPlan = new FrmPdePruebas();
            frmPlan.Show();
            return;
        }

        private void btnCdePruebas_Click(object sender, EventArgs e)
        {
            this.Close();
            var frmCasos = new FrmCdePruebas();
            frmCasos.Show();
            return;
        }
        private void IniciarDataGridView()
        {
            // Cree un DataGridView no vinculado declarando un recuento de columnas.
            dgvProyectos.ColumnCount = 5;
            dgvProyectos.ColumnHeadersVisible = true;

            // Configuramos la AutoGenerateColumns en false para que no se autogeneren las columnas
            dgvProyectos.AutoGenerateColumns = false;

            // Cambia el estilo de la cabecera de la grilla.
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();

            columnHeaderStyle.BackColor = Color.Beige;
            columnHeaderStyle.Font = new Font("Verdana", 8, FontStyle.Bold);
            dgvProyectos.ColumnHeadersDefaultCellStyle = columnHeaderStyle;


            // Definimos el ancho de la columna.
            dgvProyectos.Columns[0].Name = "Descripcion de Proyectos";
            dgvProyectos.Columns[0].DataPropertyName = "Descripcion"; 

            dgvProyectos.Columns[1].Name = "Version";
            dgvProyectos.Columns[1].DataPropertyName = "Version";

            dgvProyectos.Columns[2].Name = "Alcance";
            dgvProyectos.Columns[2].DataPropertyName = "Alcance";

            dgvProyectos.Columns[3].Name = "Responsable";
            dgvProyectos.Columns[3].DataPropertyName = "NombreResponsable";

            dgvProyectos.Columns[4].Name = "Producto";
            dgvProyectos.Columns[4].DataPropertyName = "NombreProducto";
            


            dgvProyectos.Enabled = false;

            // Cambia el tamaño de la altura de los encabezados de columna.
            dgvProyectos.AutoResizeColumnHeadersHeight();

            // Cambia el tamaño de todas las alturas de fila para ajustar el contenido de todas las celdas que no sean de encabezado.
            dgvProyectos.AutoResizeRows(
                DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);


            
            SetearContadorRegistros(0);

        }

        private void chkObtenerTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (chkObtenerTodos.Checked)
            {
                txtProyectos.Enabled = false;
                txtProyectos.Text = null;
                chkDadosBaja.Enabled = false;
                btnConsultar.Enabled = true;
                
                cmbProducto.Enabled = false;
                cmbResponsable.Enabled = false;

            }
            else
            {
                txtProyectos.Enabled = true;
                chkObtenerTodos.Enabled = true;
                chkDadosBaja.Enabled = true;
                
             
                cmbProducto.Enabled = true;
                cmbResponsable.Enabled = true;

            }
        }

        private void chkDadosBaja_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDadosBaja.Checked)
            {
                txtProyectos.Enabled = false;
                txtProyectos.Text = null;
                chkObtenerTodos.Enabled = false;
                btnConsultar.Enabled = true;
                
                cmbProducto.Enabled = false;
                cmbResponsable.Enabled = false;

            }
            else
            {
                chkObtenerTodos.Enabled = true;
                txtProyectos.Enabled = true;
                chkDadosBaja.Enabled = true;
                
                
                cmbResponsable.Enabled = true;
                cmbProducto.Enabled = true;
            }
        }

        private void FrmProyecto_Load(object sender, EventArgs e)
        {

            IniciarDataGridView();
            var productos = _productoServicio.ObtenerProductos();
            var usuarios = _usuarioServicio.ObtenerUsuarios();

            
            LlenarCombo(cmbProducto, productos, "NombreProducto", "IdProducto");
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
        }
        
        
        private void LlenarCombo(ComboBox cmb, Object lista, string display, string value)
        {

            cmb.DataSource = lista;
            cmb.DisplayMember = display;
            cmb.ValueMember = value;
            cmb.SelectedIndex = -1;
        }



        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            dgvProyectos.DataSource = null;
            dgvProyectos.Enabled = false;
            chkObtenerTodos.Enabled = true;
            chkDadosBaja.Checked = false;
            chkObtenerTodos.Checked = false;
            chkDadosBaja.Enabled = true;
            txtProyectos.Enabled = true;
            txtProyectos.Text = null;
            cmbResponsable.SelectedIndex = -1;
            
            cmbProducto.SelectedIndex = -1;
            SetearContadorRegistros(0);
        }

        
        private void dgvProyectos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnActualizar.Enabled = true;
            btnEliminar.Enabled = true;
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var formulario = new FrmABMProyectos();
            formulario.ShowDialog();
            btnConsultar_Click(sender, e);
        }

        private void btnActualizar_Click_1(object sender, EventArgs e)
        {
            var formulario = new FrmABMProyectos();
            if (dgvProyectos.CurrentRow != null)
            {
                var proyecto = (Proyecto)dgvProyectos.CurrentRow.DataBoundItem;
                if (proyecto != null)
                {
                    if (proyecto.Borrado == true)
                    {
                        MessageBox.Show("No se puede editar un producto borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        formulario.InicializarFormulario(FormMode.Actualizar, proyecto);
                        formulario.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            var formulario = new FrmABMProyectos();
            if (dgvProyectos.CurrentRow != null)
            {
                var proyecto = (Proyecto)dgvProyectos.CurrentRow.DataBoundItem;
                if (proyecto != null)
                {
                    if (proyecto.Borrado == true)
                    {
                        MessageBox.Show("No se puede eliminar un producto ya borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        formulario.InicializarFormulario(FormMode.Eliminar, proyecto);
                        formulario.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void btnAgregar_MouseHover(object sender, EventArgs e)
        {
            var toolTipAgregar = new ToolTip();
            toolTipAgregar.SetToolTip(btnAgregar, "Agregar");
        }

        private void btnActualizar_MouseHover(object sender, EventArgs e)
        {
            var tooltipActualizar = new ToolTip();
            tooltipActualizar.SetToolTip(btnActualizar, "Actualizar");
        }

        private void btnEliminar_MouseHover(object sender, EventArgs e)
        {
            var tooltipEliminar = new ToolTip();
            tooltipEliminar.SetToolTip(btnEliminar, "Eliminar");
        }

        private void btnCiclosDePrueba_Click(object sender, EventArgs e)
        {
            var frmCiclosPrueba = new FrmCicloDePrueba();
            frmCiclosPrueba.Show();
            this.Close();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            var frmReportes = new FrmReportes();
            frmReportes.Show();
            this.Close();
        }

        private void btnVolver_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
            var frmPrincipal = new FrmPrincipal();
            frmPrincipal.Show();
            return;
        }

        private void btnSalirYDesc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                this.Close();
                var login = new FrmLogin();
                login.ShowDialog();
                return;
            }
        }
    }

}
