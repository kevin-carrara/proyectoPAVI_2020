﻿
using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmLogin : Form
    {
        private readonly UsuarioServicio _usuarioServicio;

        public FrmLogin()
        {
            InitializeComponent();
            _usuarioServicio = new UsuarioServicio();

        }

        private void txtuser_Enter(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "Usuario")
            {
                txtUsuario.Text = "";
                txtUsuario.ForeColor = Color.Black;
            }
        }

        private void txtuser_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
            {
                txtUsuario.Text = "Usuario";
                txtUsuario.ForeColor = Color.Black;

            }
        }

        private void txtpass_Enter(object sender, EventArgs e)
        {
            if (txtContraseña.Text == "Contraseña")
                txtContraseña.Text = "";
            txtContraseña.ForeColor = Color.Black;
            txtContraseña.UseSystemPasswordChar = true;
        }

        private void txtpass_Leave(object sender, EventArgs e)
        {
            if (txtContraseña.Text == "")
            {
                txtContraseña.Text = "Contraseña";
                txtContraseña.ForeColor = Color.Black;
                txtContraseña.UseSystemPasswordChar = false;
            }
        }

        private void btncerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            var user = _usuarioServicio.ValidarUsuarioYContraseña(txtUsuario.Text, txtContraseña.Text);

            if (user != null)
            {
                UsuarioLogueado.NombreUsuario = user.NombreUsuario;
                var frmPrincipal = new FrmPrincipal();
                frmPrincipal.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Usuario y/o contraseña invalidos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnRecuperar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var frmRecuperar = new FrmRecuperacion();
            frmRecuperar.Show();
            this.Close();

        }

        private void txtContraseña_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                var user = _usuarioServicio.ValidarUsuarioYContraseña(txtUsuario.Text, txtContraseña.Text);
                if (user != null)
                {
                    UsuarioLogueado.NombreUsuario = user.NombreUsuario;
                    var frmPrincipal = new FrmPrincipal();
                    frmPrincipal.Show();
                    this.Close();
                }
                else if (txtUsuario.Text == null || txtContraseña.Text == null)
                {
                    MessageBox.Show("Usuario y/o contraseña invalidos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if (txtUsuario.Text == "Usuario" && txtContraseña.Text.Length == 0)
                {
                    MessageBox.Show("Usuario y/o contraseña invalidos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if(user == null)
                {
                    MessageBox.Show("Usuario y/o contraseña invalidos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                var user = _usuarioServicio.ValidarUsuarioYContraseña(txtUsuario.Text, txtContraseña.Text);
                if (user != null)
                {
                    UsuarioLogueado.NombreUsuario = user.NombreUsuario;
                    var frmPrincipal = new FrmPrincipal();
                    frmPrincipal.Show();
                    this.Close();
                }
                if (txtContraseña.Text == "Contraseña" && txtUsuario.Text.Length == 0)
                {
                    MessageBox.Show("Usuario y/o contraseña invalidos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
                else if (txtUsuario.Text != null && txtContraseña.Text == "Contraseña")
                {
                    MessageBox.Show("Usuario y/o contraseña invalidos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if (user == null)
                {
                    MessageBox.Show("Usuario y/o contraseña invalidos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }
    }
}

