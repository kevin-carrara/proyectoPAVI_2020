﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmCicloDePrueba : Form
    {
        private PlanesDePruebas _planPruebaSeleccionado;
        private readonly UsuarioServicio _usuarioServicio;
        private readonly CiclosDePruebaServicio _ciclosDePruebaServicio;
        public FrmCicloDePrueba()
        {
            InitializeComponent();
            lblUsuarioLogueado.Text = UsuarioLogueado.NombreUsuario;
            _ciclosDePruebaServicio = new CiclosDePruebaServicio();
            _usuarioServicio = new UsuarioServicio();
            _ciclosDePruebaServicio = new CiclosDePruebaServicio();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }


        public void InicializarFormulario(PlanesDePruebas planSeleccionado)
        {
            _planPruebaSeleccionado = planSeleccionado;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnVolver_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var frmPlanesPrueba = new FrmPdePruebas();
            frmPlanesPrueba.Show();
            this.Close();
        }

        private void btnSalirYDesc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea cerrar sesion?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                this.Close();
                var login = new FrmLogin();
                login.ShowDialog();
                return;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            dgvCiclosPrueba.DataSource = null;
            dgvCiclosPrueba.Enabled = false;
            cmbResponsable.SelectedIndex = -1;
            cmbResponsable.SelectedIndex = -1;
            rbTodos.Enabled = true;
            rbDadosBaja.Checked = false;
            rbTodos.Checked = false;
            rbDadosBaja.Enabled = true;
            SetearContadorRegistros(0);
        }

        private void SetearContadorRegistros(int total)
        {
            lblContadorRegistros.Text = null;
            lblContadorRegistros.Text = $"Total de ciclos de prueba: {total}";
        }

        private void FrmCicloDePrueba_Load(object sender, EventArgs e)
        {
            IniciarDataGridView();
            var usuarios = _usuarioServicio.ObtenerUsuarios();
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
            var listPlanPrueba = new List<PlanesDePruebas>();
            listPlanPrueba.Add(_planPruebaSeleccionado);
            LlenarCombo(cmbPlanPrueba, listPlanPrueba, "Nombre", "IdPlanDePrueba");
            cmbPlanPrueba.SelectedValue = _planPruebaSeleccionado.IdPlanDePrueba;
            cmbPlanPrueba.Enabled = false;
            SetearContadorRegistros(0);
        }

        private void rbTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTodos.Checked)
            {
                rbDadosBaja.Enabled = false;
                btnConsultar.Enabled = true;
                cmbResponsable.SelectedIndex = -1;
                cmbResponsable.Enabled = false;

            }
            else
            {

                rbDadosBaja.Enabled = true;
                cmbResponsable.Enabled = true;
            }
        }

        private void rbDadosBaja_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDadosBaja.Checked)
            {
                rbTodos.Enabled = false;
                btnConsultar.Enabled = true;
                cmbResponsable.SelectedIndex = -1;
                cmbResponsable.Enabled = false;

            }
            else
            {
                rbDadosBaja.Enabled = true;
                cmbResponsable.Enabled = true;

            }
        }

        private void LlenarCombo(ComboBox cbo, Object source, string display, String value)
        {
            cbo.DataSource = source;
            cbo.DisplayMember = display;
            cbo.ValueMember = value;
            cbo.SelectedIndex = -1;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            var idPlanSeleccionado = _planPruebaSeleccionado.IdPlanDePrueba;
            dgvCiclosPrueba.Enabled = true;

            if (!rbDadosBaja.Checked && !rbTodos.Checked)
            {

                dgvCiclosPrueba.DataSource = _ciclosDePruebaServicio.BuscarConFiltros((int?)cmbResponsable.SelectedValue, idPlanSeleccionado);
                SetearContadorRegistros(dgvCiclosPrueba.Rows.Count);
            }
            else if (rbDadosBaja.Checked)
            {
                dgvCiclosPrueba.DataSource = _ciclosDePruebaServicio.ObtenerDadosBaja(idPlanSeleccionado);
                SetearContadorRegistros(dgvCiclosPrueba.Rows.Count);

                foreach (DataGridViewRow dgvr in dgvCiclosPrueba.Rows)
                {

                    var planDePrueba = (CicloDePrueba)dgvCiclosPrueba.CurrentRow.DataBoundItem;
                    if (planDePrueba.Borrado == true)
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.Red;
                    }
                    else
                    {
                        dgvr.DefaultCellStyle.ForeColor = Color.Black;
                    }
                }

            }
            else
            {
                dgvCiclosPrueba.DataSource = _ciclosDePruebaServicio.ObtenerTodos(idPlanSeleccionado);
                SetearContadorRegistros(dgvCiclosPrueba.Rows.Count);
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var frmTransaccionAgregar = new FrmTransaccionCiclosPrueba();
            frmTransaccionAgregar.InicializarFormulario(_planPruebaSeleccionado, FormMode.Nuevo);
            frmTransaccionAgregar.Show();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmTransaccionCiclosPrueba();
            if (dgvCiclosPrueba.CurrentRow != null)
            {
                var cicloPrueba = (CicloDePrueba)dgvCiclosPrueba.CurrentRow.DataBoundItem;
                if (cicloPrueba != null)
                {
                    if (cicloPrueba.Borrado == true)
                    {
                        MessageBox.Show("No se puede editar un ciclo de prueba borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        formulario.InicializarFormularioConCiclo(cicloPrueba,_planPruebaSeleccionado, FormMode.Actualizar);
                        formulario.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            var transaccionCiclo = new FrmTransaccionCiclosPrueba();
            if (dgvCiclosPrueba.CurrentRow != null)
            {
                var cicloPrueba = (CicloDePrueba)dgvCiclosPrueba.CurrentRow.DataBoundItem;
                if (cicloPrueba != null)
                {
                    if (cicloPrueba.Borrado == true)
                    {
                        MessageBox.Show("No se puede eliminar un plan de prueba ya borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        transaccionCiclo.InicializarFormularioConCiclo(cicloPrueba,_planPruebaSeleccionado, FormMode.Eliminar);
                        transaccionCiclo.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void IniciarDataGridView()
        {
            // Cree un DataGridView no vinculado declarando un recuento de columnas.
            dgvCiclosPrueba.ColumnCount = 5;
            dgvCiclosPrueba.ColumnHeadersVisible = true;

            // Configuramos la AutoGenerateColumns en false para que no se autogeneren las columnas
            dgvCiclosPrueba.AutoGenerateColumns = false;

            // Cambia el estilo de la cabecera de la grilla.
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();

            columnHeaderStyle.BackColor = Color.Beige;
            columnHeaderStyle.Font = new Font("Verdana", 8, FontStyle.Bold);
            dgvCiclosPrueba.ColumnHeadersDefaultCellStyle = columnHeaderStyle;


            // Definimos el ancho de la columna.
            dgvCiclosPrueba.Columns[0].Name = "Plan de prueba";
            dgvCiclosPrueba.Columns[0].DataPropertyName = "NombrePlanPrueba";

            dgvCiclosPrueba.Columns[1].Name = "Responsable";
            dgvCiclosPrueba.Columns[1].DataPropertyName = "NombreResponsable";

            dgvCiclosPrueba.Columns[2].Name = "Fecha inicio de ejecucion";
            dgvCiclosPrueba.Columns[2].DataPropertyName = "FechaInicioEjecucion";

            dgvCiclosPrueba.Columns[3].Name = "Fecha fin de ejecucion";
            dgvCiclosPrueba.Columns[3].DataPropertyName = "FechaFinEjecucion";

            dgvCiclosPrueba.Columns[4].Name = "Aceptado";
            dgvCiclosPrueba.Columns[4].DataPropertyName = "Aceptado";

            dgvCiclosPrueba.Enabled = false;
            dgvCiclosPrueba.ShowCellToolTips = false;

            // Cambia el tamaño de la altura de los encabezados de columna.
            dgvCiclosPrueba.AutoResizeColumnHeadersHeight();

            // Cambia el tamaño de todas las alturas de fila para ajustar el contenido de todas las celdas que no sean de encabezado.
            dgvCiclosPrueba.AutoResizeRows(
                DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);

        }
    }
}
