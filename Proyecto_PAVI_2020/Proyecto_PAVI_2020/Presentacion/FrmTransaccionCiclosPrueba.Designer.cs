﻿namespace Proyecto_PAVI_2020.Presentacion
{
    partial class FrmTransaccionCiclosPrueba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTransaccionCiclosPrueba));
            this.lblResponsable = new System.Windows.Forms.Label();
            this.C = new System.Windows.Forms.Label();
            this.lblCicloPrueba = new System.Windows.Forms.Label();
            this.cmbResponsable = new System.Windows.Forms.ComboBox();
            this.cmbPlanPrueba = new System.Windows.Forms.ComboBox();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.dtpFin = new System.Windows.Forms.DateTimePicker();
            this.lblFechaInicio = new System.Windows.Forms.Label();
            this.lblFechaFin = new System.Windows.Forms.Label();
            this.lblAceptado = new System.Windows.Forms.Label();
            this.cmbAceptado = new System.Windows.Forms.ComboBox();
            this.lblDetalle = new System.Windows.Forms.Label();
            this.lblUsuarioTester = new System.Windows.Forms.Label();
            this.cmbTester = new System.Windows.Forms.ComboBox();
            this.cmbCasoPrueba = new System.Windows.Forms.ComboBox();
            this.lblCasoPrueba = new System.Windows.Forms.Label();
            this.lblHoras = new System.Windows.Forms.Label();
            this.txtHoras = new System.Windows.Forms.TextBox();
            this.dtpFechaEjecucionDetalle = new System.Windows.Forms.DateTimePicker();
            this.lblFechaEjecucion = new System.Windows.Forms.Label();
            this.cmbAceptadoDetalle = new System.Windows.Forms.ComboBox();
            this.lblAceptadoDetalle = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblTransaccion = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblResponsable
            // 
            this.lblResponsable.AutoSize = true;
            this.lblResponsable.Location = new System.Drawing.Point(188, 89);
            this.lblResponsable.Name = "lblResponsable";
            this.lblResponsable.Size = new System.Drawing.Size(72, 13);
            this.lblResponsable.TabIndex = 96;
            this.lblResponsable.Text = "Responsable:";
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.Location = new System.Drawing.Point(178, 116);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(82, 13);
            this.C.TabIndex = 99;
            this.C.Text = "Plan de prueba:";
            // 
            // lblCicloPrueba
            // 
            this.lblCicloPrueba.AutoSize = true;
            this.lblCicloPrueba.Location = new System.Drawing.Point(76, 54);
            this.lblCicloPrueba.Name = "lblCicloPrueba";
            this.lblCicloPrueba.Size = new System.Drawing.Size(84, 13);
            this.lblCicloPrueba.TabIndex = 100;
            this.lblCicloPrueba.Text = "Ciclo de prueba:";
            // 
            // cmbResponsable
            // 
            this.cmbResponsable.FormattingEnabled = true;
            this.cmbResponsable.ItemHeight = 13;
            this.cmbResponsable.Location = new System.Drawing.Point(270, 81);
            this.cmbResponsable.Name = "cmbResponsable";
            this.cmbResponsable.Size = new System.Drawing.Size(172, 21);
            this.cmbResponsable.TabIndex = 1;
            // 
            // cmbPlanPrueba
            // 
            this.cmbPlanPrueba.FormattingEnabled = true;
            this.cmbPlanPrueba.Location = new System.Drawing.Point(270, 108);
            this.cmbPlanPrueba.Name = "cmbPlanPrueba";
            this.cmbPlanPrueba.Size = new System.Drawing.Size(172, 21);
            this.cmbPlanPrueba.TabIndex = 2;
            // 
            // dtpInicio
            // 
            this.dtpInicio.Location = new System.Drawing.Point(270, 135);
            this.dtpInicio.MaxDate = new System.DateTime(2020, 10, 25, 23, 59, 59, 0);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.Size = new System.Drawing.Size(172, 20);
            this.dtpInicio.TabIndex = 3;
            // 
            // dtpFin
            // 
            this.dtpFin.Location = new System.Drawing.Point(270, 161);
            this.dtpFin.Name = "dtpFin";
            this.dtpFin.Size = new System.Drawing.Size(172, 20);
            this.dtpFin.TabIndex = 4;
            // 
            // lblFechaInicio
            // 
            this.lblFechaInicio.AutoSize = true;
            this.lblFechaInicio.Location = new System.Drawing.Point(133, 142);
            this.lblFechaInicio.Name = "lblFechaInicio";
            this.lblFechaInicio.Size = new System.Drawing.Size(131, 13);
            this.lblFechaInicio.TabIndex = 105;
            this.lblFechaInicio.Text = "Fecha inicio de ejecucion:";
            // 
            // lblFechaFin
            // 
            this.lblFechaFin.AutoSize = true;
            this.lblFechaFin.Location = new System.Drawing.Point(146, 167);
            this.lblFechaFin.Name = "lblFechaFin";
            this.lblFechaFin.Size = new System.Drawing.Size(118, 13);
            this.lblFechaFin.TabIndex = 106;
            this.lblFechaFin.Text = "Fecha fin de ejecucion:";
            // 
            // lblAceptado
            // 
            this.lblAceptado.AutoSize = true;
            this.lblAceptado.Location = new System.Drawing.Point(204, 196);
            this.lblAceptado.Name = "lblAceptado";
            this.lblAceptado.Size = new System.Drawing.Size(56, 13);
            this.lblAceptado.TabIndex = 107;
            this.lblAceptado.Text = "Aceptado:";
            // 
            // cmbAceptado
            // 
            this.cmbAceptado.FormattingEnabled = true;
            this.cmbAceptado.Items.AddRange(new object[] {
            "Si ",
            "No"});
            this.cmbAceptado.Location = new System.Drawing.Point(270, 188);
            this.cmbAceptado.Name = "cmbAceptado";
            this.cmbAceptado.Size = new System.Drawing.Size(172, 21);
            this.cmbAceptado.TabIndex = 5;
            // 
            // lblDetalle
            // 
            this.lblDetalle.AutoSize = true;
            this.lblDetalle.Location = new System.Drawing.Point(117, 264);
            this.lblDetalle.Name = "lblDetalle";
            this.lblDetalle.Size = new System.Drawing.Size(43, 13);
            this.lblDetalle.TabIndex = 109;
            this.lblDetalle.Text = "Detalle:";
            // 
            // lblUsuarioTester
            // 
            this.lblUsuarioTester.AutoSize = true;
            this.lblUsuarioTester.Location = new System.Drawing.Point(220, 321);
            this.lblUsuarioTester.Name = "lblUsuarioTester";
            this.lblUsuarioTester.Size = new System.Drawing.Size(40, 13);
            this.lblUsuarioTester.TabIndex = 110;
            this.lblUsuarioTester.Text = "Tester:";
            // 
            // cmbTester
            // 
            this.cmbTester.FormattingEnabled = true;
            this.cmbTester.Location = new System.Drawing.Point(270, 313);
            this.cmbTester.Name = "cmbTester";
            this.cmbTester.Size = new System.Drawing.Size(172, 21);
            this.cmbTester.TabIndex = 7;
            // 
            // cmbCasoPrueba
            // 
            this.cmbCasoPrueba.FormattingEnabled = true;
            this.cmbCasoPrueba.Location = new System.Drawing.Point(270, 286);
            this.cmbCasoPrueba.Name = "cmbCasoPrueba";
            this.cmbCasoPrueba.Size = new System.Drawing.Size(172, 21);
            this.cmbCasoPrueba.TabIndex = 6;
            // 
            // lblCasoPrueba
            // 
            this.lblCasoPrueba.AutoSize = true;
            this.lblCasoPrueba.Location = new System.Drawing.Point(175, 294);
            this.lblCasoPrueba.Name = "lblCasoPrueba";
            this.lblCasoPrueba.Size = new System.Drawing.Size(85, 13);
            this.lblCasoPrueba.TabIndex = 113;
            this.lblCasoPrueba.Text = "Caso de prueba:";
            // 
            // lblHoras
            // 
            this.lblHoras.AutoSize = true;
            this.lblHoras.Location = new System.Drawing.Point(164, 347);
            this.lblHoras.Name = "lblHoras";
            this.lblHoras.Size = new System.Drawing.Size(96, 13);
            this.lblHoras.TabIndex = 114;
            this.lblHoras.Text = "Cantidad de horas:";
            // 
            // txtHoras
            // 
            this.txtHoras.Location = new System.Drawing.Point(270, 340);
            this.txtHoras.MaxLength = 50;
            this.txtHoras.Name = "txtHoras";
            this.txtHoras.Size = new System.Drawing.Size(172, 20);
            this.txtHoras.TabIndex = 8;
            this.txtHoras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHoras_KeyPress);
            // 
            // dtpFechaEjecucionDetalle
            // 
            this.dtpFechaEjecucionDetalle.Location = new System.Drawing.Point(270, 366);
            this.dtpFechaEjecucionDetalle.Name = "dtpFechaEjecucionDetalle";
            this.dtpFechaEjecucionDetalle.Size = new System.Drawing.Size(172, 20);
            this.dtpFechaEjecucionDetalle.TabIndex = 9;
            // 
            // lblFechaEjecucion
            // 
            this.lblFechaEjecucion.AutoSize = true;
            this.lblFechaEjecucion.Location = new System.Drawing.Point(156, 373);
            this.lblFechaEjecucion.Name = "lblFechaEjecucion";
            this.lblFechaEjecucion.Size = new System.Drawing.Size(104, 13);
            this.lblFechaEjecucion.TabIndex = 117;
            this.lblFechaEjecucion.Text = "Fecha de ejecucion:";
            // 
            // cmbAceptadoDetalle
            // 
            this.cmbAceptadoDetalle.FormattingEnabled = true;
            this.cmbAceptadoDetalle.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.cmbAceptadoDetalle.Location = new System.Drawing.Point(270, 392);
            this.cmbAceptadoDetalle.Name = "cmbAceptadoDetalle";
            this.cmbAceptadoDetalle.Size = new System.Drawing.Size(172, 21);
            this.cmbAceptadoDetalle.TabIndex = 9;
            // 
            // lblAceptadoDetalle
            // 
            this.lblAceptadoDetalle.AutoSize = true;
            this.lblAceptadoDetalle.Location = new System.Drawing.Point(204, 400);
            this.lblAceptadoDetalle.Name = "lblAceptadoDetalle";
            this.lblAceptadoDetalle.Size = new System.Drawing.Size(56, 13);
            this.lblAceptadoDetalle.TabIndex = 119;
            this.lblAceptadoDetalle.Text = "Aceptado:";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(167, 467);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 10;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(421, 467);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.panel3.Controls.Add(this.btnMinimizar);
            this.panel3.Controls.Add(this.btnCerrar);
            this.panel3.Location = new System.Drawing.Point(223, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(430, 29);
            this.panel3.TabIndex = 125;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.btnMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Image")));
            this.btnMinimizar.Location = new System.Drawing.Point(315, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(40, 29);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 123;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(352, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(36, 29);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 124;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.panel8.Controls.Add(this.lblTransaccion);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(260, 29);
            this.panel8.TabIndex = 122;
            // 
            // lblTransaccion
            // 
            this.lblTransaccion.AutoSize = true;
            this.lblTransaccion.BackColor = System.Drawing.Color.Transparent;
            this.lblTransaccion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTransaccion.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransaccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.lblTransaccion.Location = new System.Drawing.Point(3, 9);
            this.lblTransaccion.Name = "lblTransaccion";
            this.lblTransaccion.Size = new System.Drawing.Size(203, 17);
            this.lblTransaccion.TabIndex = 15;
            this.lblTransaccion.Text = "Agregar ciclo de prueba - detalle";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.panel2.Location = new System.Drawing.Point(0, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(12, 489);
            this.panel2.TabIndex = 126;
            // 
            // FrmTransaccionCiclosPrueba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(610, 516);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.lblAceptadoDetalle);
            this.Controls.Add(this.cmbAceptadoDetalle);
            this.Controls.Add(this.lblFechaEjecucion);
            this.Controls.Add(this.dtpFechaEjecucionDetalle);
            this.Controls.Add(this.txtHoras);
            this.Controls.Add(this.lblHoras);
            this.Controls.Add(this.lblCasoPrueba);
            this.Controls.Add(this.cmbCasoPrueba);
            this.Controls.Add(this.cmbTester);
            this.Controls.Add(this.lblUsuarioTester);
            this.Controls.Add(this.lblDetalle);
            this.Controls.Add(this.cmbAceptado);
            this.Controls.Add(this.lblAceptado);
            this.Controls.Add(this.lblFechaFin);
            this.Controls.Add(this.lblFechaInicio);
            this.Controls.Add(this.dtpFin);
            this.Controls.Add(this.dtpInicio);
            this.Controls.Add(this.cmbPlanPrueba);
            this.Controls.Add(this.cmbResponsable);
            this.Controls.Add(this.lblCicloPrueba);
            this.Controls.Add(this.C);
            this.Controls.Add(this.lblResponsable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmTransaccionCiclosPrueba";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmTransaccionCiclosPrueba";
            this.Load += new System.EventHandler(this.FrmTransaccionCiclosPrueba_Load);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblResponsable;
        private System.Windows.Forms.Label C;
        private System.Windows.Forms.Label lblCicloPrueba;
        private System.Windows.Forms.ComboBox cmbResponsable;
        private System.Windows.Forms.ComboBox cmbPlanPrueba;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.DateTimePicker dtpFin;
        private System.Windows.Forms.Label lblFechaInicio;
        private System.Windows.Forms.Label lblFechaFin;
        private System.Windows.Forms.Label lblAceptado;
        private System.Windows.Forms.ComboBox cmbAceptado;
        private System.Windows.Forms.Label lblDetalle;
        private System.Windows.Forms.Label lblUsuarioTester;
        private System.Windows.Forms.ComboBox cmbTester;
        private System.Windows.Forms.ComboBox cmbCasoPrueba;
        private System.Windows.Forms.Label lblCasoPrueba;
        private System.Windows.Forms.Label lblHoras;
        private System.Windows.Forms.TextBox txtHoras;
        private System.Windows.Forms.DateTimePicker dtpFechaEjecucionDetalle;
        private System.Windows.Forms.Label lblFechaEjecucion;
        private System.Windows.Forms.ComboBox cmbAceptadoDetalle;
        private System.Windows.Forms.Label lblAceptadoDetalle;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblTransaccion;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.Panel panel2;
    }
}