﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmABMProyectos : Form
    {
        private FormMode formMode = FormMode.Nuevo;
        private readonly ProyectoServicio _proyectoServicio;
        private readonly ProductosServicio _productosServicio;
        private readonly UsuarioServicio _usuarioServicio;
        private Proyecto _proyectoSeleccionado;
        public FrmABMProyectos()
        {
            InitializeComponent();
            _usuarioServicio = new UsuarioServicio();
            _productosServicio = new ProductosServicio();
            _proyectoServicio = new ProyectoServicio();
        }
        public void InicializarFormulario(FormMode op, Proyecto proyectoSelected)
        {
            formMode = op;
            _proyectoSeleccionado = proyectoSelected;
        }


        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        private void btnAceptar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.btnAceptar_Click(sender, e);

        }

        private void btnCancelar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
                this.Close();
        }
        private void MostrarDatos()
        {
            if (_proyectoSeleccionado != null)
            {
                txtDescripcion.Text = _proyectoSeleccionado.Descripcion;
                txtAlcance.Text = _proyectoSeleccionado.Alcance;
                txtVersion.Text = _proyectoSeleccionado.Version;

                cmbproductos.Text = _proyectoSeleccionado.NombreProducto;
                cmbResponsable.Text = _proyectoSeleccionado.NombreResponsable;

            }
        }
        private void LlenarCombo(ComboBox cmb, Object lista, string display, string value)
        {

            cmb.DataSource = lista;
            cmb.DisplayMember = display;
            cmb.ValueMember = value;
            cmb.SelectedIndex = -1;
        }


        private void FrmABMProyectos_Load(object sender, EventArgs e)
        {
            var productos = _productosServicio.ObtenerProductos();
            var usuarios = _usuarioServicio.ObtenerUsuarios();


            LlenarCombo(cmbproductos, productos, "NombreProducto", "IdProducto");
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
            switch (formMode)
            {
                case FormMode.Nuevo:
                    {

                        txtABMProyecto.Text = "Agregar Proyecto";
                        break;
                    }
                case FormMode.Actualizar:
                    {
                        txtABMProyecto.Text = "Actualizar Proyecto";
                        MostrarDatos();

                        txtDescripcion.Enabled = true;
                        txtAlcance.Enabled = true;
                        txtVersion.Enabled = true;
                        cmbResponsable.Enabled = true;
                        cmbproductos.Enabled = true;

                        break;
                    }

                case FormMode.Eliminar:
                    {
                        MostrarDatos();
                        txtABMProyecto.Text = "Eliminar Proyecto";
                        txtDescripcion.Enabled = false;
                        txtAlcance.Enabled = false;
                        txtVersion.Enabled = false;
                        cmbResponsable.Enabled = false;
                        cmbproductos.Enabled = false;

                        break;
                    }
            }

        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            switch (formMode)
            {
                case FormMode.Nuevo:
                    if (!string.IsNullOrEmpty(txtDescripcion.Text) && !string.IsNullOrEmpty(txtAlcance.Text)&& !string.IsNullOrEmpty(txtVersion.Text))
                    {

                        var proyecto = SetearProyecto(null, txtDescripcion.Text, txtVersion.Text, txtAlcance.Text, (int?)cmbproductos.SelectedValue, (int?)cmbResponsable.SelectedValue);
                        if (ExisteProyecto(proyecto) == false)
                        {
                           

                            var agregado = _proyectoServicio.AgregarProyecto(proyecto);
                            if (agregado == true)
                            {
                                MessageBox.Show("Se ha agregado correctamente el Proyecto.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Dispose();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo agregar el Proyecto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No se puede agregar un Proyecto existente.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Todos los.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Actualizar:
                    if (!string.IsNullOrEmpty(txtDescripcion.Text) && cmbResponsable.SelectedIndex != -1 && cmbproductos.SelectedIndex != -1)
                    {
                        var proyecto = SetearProyecto(_proyectoSeleccionado.IdProyecto, txtDescripcion.Text, txtVersion.Text, txtAlcance.Text, (int?)cmbproductos.SelectedValue, (int?)cmbResponsable.SelectedValue);
                        if (ExisteProyecto(proyecto)== false )
                        {

                            var modificado = _proyectoServicio.ActualizarProyecto(proyecto);
                            if (modificado == true)
                            {
                                MessageBox.Show("Se ha actualizado correctamente el Proyecto.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Dispose();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo actualizar el Proyecto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                this.Dispose();
                            }
                        }
                        else
                        {
                            MessageBox.Show("El Proyecto ya existe.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else if (txtDescripcion.Text == "" || txtAlcance.Text != "" || txtVersion.Text != "")
                    {
                        MessageBox.Show("No Puede haber Campos vacios de proyecto para actualizarlo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Eliminar:


                    var eliminado = _proyectoServicio.EliminarProyecto(_proyectoSeleccionado);
                    if (eliminado == true)
                    {
                        MessageBox.Show("Se ha eliminado correctamente el  pryecto.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Dispose();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el  proyecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Dispose();
                    }
                    break;
                default:
                    break;
            }
        }
        private Proyecto SetearProyecto(int? idProyecto, string des, string ver, string alcance, int? idpro, int? idResponsable)
        {
            var proyecto = new Proyecto();
            proyecto.IdProyecto = idProyecto ?? 0;
            proyecto.Descripcion = des;
            proyecto.Version = ver;
            proyecto.Alcance = alcance;
            proyecto.IdProducto = idpro ?? null;
            proyecto.IdResponsable = idResponsable ?? null;
            proyecto.Borrado = false;
            return proyecto;
        }
        private bool ExisteProyecto(Proyecto pro)
        {
            var existe = _proyectoServicio.ExisteProyecto(pro);
            return existe;
        }


        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtVersion_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

    


