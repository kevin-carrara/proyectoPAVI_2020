﻿using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmRecuperacion : Form
    {
        private readonly UsuarioServicio _usuarioServicio;
        public FrmRecuperacion()
        {
            InitializeComponent();
            _usuarioServicio = new UsuarioServicio();
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "" && txtConfirm.Text != "" && txtContra.Text != "")
            {
                if (txtContra.Text == txtConfirm.Text)
                {
                    var contra = _usuarioServicio.RecuperarContraseña(txtNombre.Text, txtContra.Text);
                    if (contra == true)
                    {
                        MessageBox.Show("Contraseña restablecida", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        var login = new FrmLogin();
                        login.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se restablecio su contraseña", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Las Contraseñas no coinciden ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
            }
            else
            {
                MessageBox.Show("Los campos no pueden estar vacios", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            var login = new FrmLogin();
            login.Show();
            this.Close();
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtContra_Enter(object sender, EventArgs e)
        {
            txtContra.UseSystemPasswordChar = true;
        }

        private void txtConfirm_Enter(object sender, EventArgs e)
        {
            txtConfirm.UseSystemPasswordChar = true;
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
