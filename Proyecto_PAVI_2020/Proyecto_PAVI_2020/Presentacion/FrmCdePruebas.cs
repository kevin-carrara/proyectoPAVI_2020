﻿using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmCdePruebas : Form
    {
        private readonly CasosDePruebaServicio _casosDePruebaServicio;
        private readonly PlanesDePruebaServicio _planesPruebaServicio;
        private readonly UsuarioServicio _usuarioServicio;
        public FrmCdePruebas()
        {
            InitializeComponent();
            lblUsuarioLogueado.Text = UsuarioLogueado.NombreUsuario;
            _casosDePruebaServicio = new CasosDePruebaServicio();
            _planesPruebaServicio = new PlanesDePruebaServicio();
            _usuarioServicio = new UsuarioServicio();

        }

        private void btnProyectos_Click(object sender, EventArgs e)
        {
            var frmProyectos = new FrmProyecto();
            frmProyectos.Show();
            this.Close();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            var frmProductos = new FrmProductos();
            frmProductos.Show();
            this.Close();
        }

        private void btnPlanesdePrueba_Click(object sender, EventArgs e)
        {
            var frmPlanesPrueba = new FrmPdePruebas();
            frmPlanesPrueba.Show();
            this.Close();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            var frmReportes = new FrmReportes();
            frmReportes.Show();
            this.Close();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnVolver_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var frmPrincipal = new FrmPrincipal();
            frmPrincipal.Show();
            this.Close();
        }

        private void btnSalirYDesc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
            var frmLogin = new FrmLogin();
            frmLogin.Show();
            this.Close();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }

        private void IniciarDataGridView()
        {
            // Cree un DataGridView no vinculado declarando un recuento de columnas.
            dgvCasosPrueba.ColumnCount = 4;
            dgvCasosPrueba.ColumnHeadersVisible = true;

            // Configuramos la AutoGenerateColumns en false para que no se autogeneren las columnas
            dgvCasosPrueba.AutoGenerateColumns = false;

            // Cambia el estilo de la cabecera de la grilla.
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();

            columnHeaderStyle.BackColor = Color.Beige;
            columnHeaderStyle.Font = new Font("Verdana", 8, FontStyle.Bold);
            dgvCasosPrueba.ColumnHeadersDefaultCellStyle = columnHeaderStyle;


            // Definimos el ancho de la columna.
            dgvCasosPrueba.Columns[0].Name = "Titulo";
            dgvCasosPrueba.Columns[0].DataPropertyName = "Titulo";

            dgvCasosPrueba.Columns[1].Name = "Descripcion";
            dgvCasosPrueba.Columns[1].DataPropertyName = "Descripcion";

            dgvCasosPrueba.Columns[2].Name = "Responsable";
            dgvCasosPrueba.Columns[2].DataPropertyName = "NombreResponsable";

            dgvCasosPrueba.Columns[3].Name = "Plan de prueba";
            dgvCasosPrueba.Columns[3].DataPropertyName = "NombrePlanPrueba";

            dgvCasosPrueba.Enabled = false;
            dgvCasosPrueba.ShowCellToolTips = false;

            // Cambia el tamaño de la altura de los encabezados de columna.
            dgvCasosPrueba.AutoResizeColumnHeadersHeight();

            // Cambia el tamaño de todas las alturas de fila para ajustar el contenido de todas las celdas que no sean de encabezado.
            dgvCasosPrueba.AutoResizeRows(
                DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var formulario = new FrmABMCdePruebas();
            formulario.ShowDialog();
            btnConsultar_Click(sender, e);
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            var abmCPrueba = new FrmABMCdePruebas();
            if (dgvCasosPrueba.CurrentRow != null)
            {
                var casos = (CasoDePrueba)dgvCasosPrueba.CurrentRow.DataBoundItem;
                if (casos != null)
                {
                    if (casos.Borrado == true)
                    {
                        MessageBox.Show("No se puede editar un Caso de Prueba ya borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        abmCPrueba.InicializarFormulario(FormMode.Actualizar, casos);
                        abmCPrueba.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar editar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            var abmCPrueba = new FrmABMCdePruebas();
            if (dgvCasosPrueba.CurrentRow != null)
            {
                var caso = (CasoDePrueba)dgvCasosPrueba.CurrentRow.DataBoundItem;
                if (caso != null)
                {
                    if (caso.Borrado == true)
                    {
                        MessageBox.Show("No se puede eliminar un plan de prueba ya borrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        abmCPrueba.InicializarFormulario(FormMode.Eliminar, caso);
                        abmCPrueba.ShowDialog();
                        btnConsultar_Click(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                MessageBox.Show("Debe realizar una consulta antes de intentar eliminar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void rbDadosBaja_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDadosBaja.Checked)
            {
                txtCasosPrueba.Enabled = false;
                txtCasosPrueba.Text = null;
                rbTodos.Enabled = false;
                cmbPlanPrueba.SelectedIndex = -1;
                cmbPlanPrueba.Enabled = false;
                cmbResponsable.SelectedIndex = -1;
                cmbResponsable.Enabled = false;

            }
            else
            {
                txtCasosPrueba.Enabled = true;
                txtCasosPrueba.Enabled = true;
                cmbResponsable.Enabled = true;
                cmbPlanPrueba.Enabled = true;

            }
        }

        private void rbTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTodos.Checked)
            {
                txtCasosPrueba.Enabled = false;
                txtCasosPrueba.Text = null;
                rbDadosBaja.Enabled = false;
                btnConsultar.Enabled = true;
                cmbPlanPrueba.SelectedIndex = -1;
                cmbPlanPrueba.Enabled = false;
                cmbResponsable.SelectedIndex = -1;
                cmbResponsable.Enabled = false;

            }
            else
            {
                txtCasosPrueba.Enabled = true;
                rbDadosBaja.Enabled = true;
                cmbPlanPrueba.Enabled = true;
                cmbResponsable.Enabled = true;
            }
        }

        private void FrmCdePruebas_Load(object sender, EventArgs e)
        {
            var planesPrueba = _planesPruebaServicio.ObtenerTodos();
            var usuarios = _usuarioServicio.ObtenerUsuarios();
            IniciarDataGridView();
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
            LlenarCombo(cmbPlanPrueba, planesPrueba, "Nombre", "IdPlanDePrueba");
            SetearContadorRegistros(0);
        }

        private void SetearContadorRegistros(int total)
        {
            lblContadorRegistros.Text = null;
            lblContadorRegistros.Text = $"Total de casos de prueba: {total}";
        }

        private void LlenarCombo(ComboBox cbo, Object source, string display, String value)
        {
            cbo.DataSource = source;
            cbo.DisplayMember = display;
            cbo.ValueMember = value;
            cbo.SelectedIndex = -1;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            dgvCasosPrueba.DataSource = null;
            dgvCasosPrueba.Enabled = false;
            cmbPlanPrueba.SelectedIndex = -1;
            cmbResponsable.SelectedIndex = -1;
            rbTodos.Enabled = true;
            rbDadosBaja.Checked = false;
            rbTodos.Checked = false;
            rbDadosBaja.Enabled = true;
            txtCasosPrueba.Enabled = true;
            txtCasosPrueba.Text = null;
            SetearContadorRegistros(0);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            
            dgvCasosPrueba.Enabled = true;

            if (!rbDadosBaja.Checked && !rbTodos.Checked)
            {
                dgvCasosPrueba.DataSource = _casosDePruebaServicio.ObtenerPorFiltro(txtCasosPrueba.Text, (int?)cmbResponsable.SelectedValue,(int?)cmbPlanPrueba.SelectedValue);
                SetearContadorRegistros(dgvCasosPrueba.Rows.Count);
            }
            else if (rbDadosBaja.Checked)
            {
                dgvCasosPrueba.DataSource = _casosDePruebaServicio.ObtenerBorrados();
                SetearContadorRegistros(dgvCasosPrueba.Rows.Count);

                foreach (DataGridViewRow dgvr in dgvCasosPrueba.Rows)
                {

                    var planDePrueba = (CasoDePrueba)dgvCasosPrueba.CurrentRow.DataBoundItem;
                    if (planDePrueba.Borrado == true)
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.Red;
                    }
                    else
                    {
                        dgvr.DefaultCellStyle.ForeColor = Color.Black;
                    }
                }

            }
            else
            {
                dgvCasosPrueba.DataSource = _casosDePruebaServicio.ObtenerTodos();
                SetearContadorRegistros(dgvCasosPrueba.Rows.Count);
            }
        }
    }
}
