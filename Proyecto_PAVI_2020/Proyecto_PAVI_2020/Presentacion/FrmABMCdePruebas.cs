﻿using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmABMCdePruebas : Form
    {
        private readonly UsuarioServicio _usuarioServicio;
        private readonly PlanesDePruebaServicio _planesServicio;
        private readonly CasosDePruebaServicio _casosServicio;
        private FormMode formMode = FormMode.Nuevo;
        private CasoDePrueba _casoSelecionado;
        public FrmABMCdePruebas()
        {
            InitializeComponent();
            _casosServicio = new CasosDePruebaServicio();
            _planesServicio = new PlanesDePruebaServicio();
            _usuarioServicio = new UsuarioServicio();
        }
        private void MostrarDatos()
        {
            if (_casoSelecionado != null)
            {
                txtNombreCaso.Text = _casoSelecionado.Titulo;
                txtDescripcion.Text = _casoSelecionado.Descripcion;
                cmbResponsable.SelectedValue = _casoSelecionado.IdResponsable;
                cmbPlan.SelectedValue = _casoSelecionado.IdPlanDePrueba;
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            switch (formMode)
            {
                case FormMode.Nuevo:
                    if (!string.IsNullOrEmpty(txtNombreCaso.Text) && !string.IsNullOrEmpty(txtDescripcion.Text) && cmbPlan.SelectedIndex != -1 && cmbPlan.SelectedIndex != -1)
                    {
                        var casoPrueba = SetearCasoDePrueba(null, txtNombreCaso.Text, txtDescripcion.Text, (int)cmbPlan.SelectedValue, (int?)cmbResponsable.SelectedValue);
                        if (ExisteCasoEnDb(casoPrueba) == false)
                        {

                            var agregado = _casosServicio.AgregarCasoDePrueba(casoPrueba);
                            if (agregado == true)
                            {
                                MessageBox.Show("Se ha agregado correctamente el caso de prueba.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo agregar el caso de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No se puede agregar un caso de prueba existente.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Todos los campos son requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Actualizar:
                    if (!string.IsNullOrEmpty(txtNombreCaso.Text) && !string.IsNullOrEmpty(txtDescripcion.Text) && cmbResponsable.SelectedIndex != -1 && cmbPlan.SelectedIndex != -1)
                    {
                        var caso = SetearCasoDePrueba(_casoSelecionado.IdCasoDePrueba, txtNombreCaso.Text, txtDescripcion.Text, (int)cmbPlan.SelectedValue,(int?)cmbResponsable.SelectedValue);
                        if (ExisteCasoEnDb(caso) == false)
                        {
                            var modificado = _casosServicio.ActualizarCasoPrueba(caso);
                            if (modificado == true)
                            {
                                MessageBox.Show("Se ha actualizado correctamente el caso de prueba.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo actualizar el caso de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                this.Close();
                            }
                        }
                        else
                        {
                            MessageBox.Show("El caso de prueba ya existe.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Todos los campos son requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Eliminar:
                    var eliminado = _casosServicio.EliminarCasoDePrueba(_casoSelecionado.IdCasoDePrueba);
                    if (eliminado == true)
                    {
                        MessageBox.Show("Se ha eliminado correctamente el caso de prueba.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el caso de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        public void InicializarFormulario(FormMode op, CasoDePrueba casoSelected)
        {
            formMode = op;
            _casoSelecionado = casoSelected;

        }
        private bool ExisteCasoEnDb(CasoDePrueba casoDePrueba)
        {
            var existe = _casosServicio.ExisteCasoDePruebaPorNombre(casoDePrueba);
            return existe;
        }
        private CasoDePrueba SetearCasoDePrueba(int? idCaso, string titulo, string descripcion, int idplan, int? idResponsable)
        {
            var caso = new CasoDePrueba();
            caso.IdCasoDePrueba = idCaso ?? 0;
            caso.Titulo = titulo;
            caso.Descripcion = descripcion;
            caso.IdPlanDePrueba = idplan ;
            caso.IdResponsable = idResponsable ?? null;
            caso.Borrado = false;
            return caso;
        }
        private void LlenarCombo(ComboBox cbo, Object source, string display, String value)
        {
            cbo.DataSource = source;
            cbo.DisplayMember = display;
            cbo.ValueMember = value;
            cbo.SelectedIndex = -1;
        }

        private void FrmABMCdePruebas_Load(object sender, EventArgs e)
        {
            var usuarios = _usuarioServicio.ObtenerUsuarios();
            var proyectos = _planesServicio.ObtenerTodos();
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
            LlenarCombo(cmbPlan, proyectos, "Nombre", "IdPlanDePrueba");
            switch (formMode)
            {
                case FormMode.Nuevo:
                    {
                        txtABMCasos.Text = "Agregar plan de prueba";
                        break;
                    }
                case FormMode.Actualizar:
                    {
                        txtABMCasos.Text = "Actualizar plan de prueba";
                        MostrarDatos();
                        txtNombreCaso.Enabled = true;
                        txtDescripcion.Enabled = true;
                        cmbPlan.Enabled = true;
                        cmbResponsable.Enabled = true;
                        break;
                    }

                case FormMode.Eliminar:
                    {
                        MostrarDatos();
                        txtABMCasos.Text = "Eliminar plan de prueba";
                        txtNombreCaso.Enabled = false;
                        txtDescripcion.Enabled = false;
                        cmbPlan.Enabled = false;
                        cmbResponsable.Enabled = false;
                        break;
                    }
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
