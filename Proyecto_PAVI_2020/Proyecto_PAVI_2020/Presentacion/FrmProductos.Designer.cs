﻿namespace Proyecto_PAVI_2020.Presentacion
{
    partial class FrmProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductos));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnVolver = new System.Windows.Forms.LinkLabel();
            this.btnSalirYDesc = new System.Windows.Forms.LinkLabel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnReportes = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnCasosdePrueba = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnPlanesdePrueba = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnProductos = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnProyectos = new System.Windows.Forms.Button();
            this.lblUsuarioLogueado = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.lblContadorRegistros = new System.Windows.Forms.Label();
            this.dgvProductos = new System.Windows.Forms.DataGridView();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.chkObtenerTodos = new System.Windows.Forms.CheckBox();
            this.chkDadosBaja = new System.Windows.Forms.CheckBox();
            this.lblProducto = new System.Windows.Forms.Label();
            this.txtProductos = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.panel1.Controls.Add(this.btnVolver);
            this.panel1.Controls.Add(this.btnSalirYDesc);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.btnReportes);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.btnCasosdePrueba);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.btnPlanesdePrueba);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.btnProductos);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.btnProyectos);
            this.panel1.Controls.Add(this.lblUsuarioLogueado);
            this.panel1.Controls.Add(this.pictureBox6);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(164, 491);
            this.panel1.TabIndex = 17;
            // 
            // btnVolver
            // 
            this.btnVolver.ActiveLinkColor = System.Drawing.SystemColors.ControlDark;
            this.btnVolver.AutoSize = true;
            this.btnVolver.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnVolver.LinkColor = System.Drawing.SystemColors.ControlDark;
            this.btnVolver.Location = new System.Drawing.Point(12, 428);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(37, 13);
            this.btnVolver.TabIndex = 14;
            this.btnVolver.TabStop = true;
            this.btnVolver.Text = "Volver";
            this.btnVolver.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnVolver_LinkClicked);
            // 
            // btnSalirYDesc
            // 
            this.btnSalirYDesc.ActiveLinkColor = System.Drawing.SystemColors.ControlDark;
            this.btnSalirYDesc.AutoSize = true;
            this.btnSalirYDesc.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnSalirYDesc.LinkColor = System.Drawing.SystemColors.ControlDark;
            this.btnSalirYDesc.Location = new System.Drawing.Point(12, 451);
            this.btnSalirYDesc.Name = "btnSalirYDesc";
            this.btnSalirYDesc.Size = new System.Drawing.Size(108, 13);
            this.btnSalirYDesc.TabIndex = 15;
            this.btnSalirYDesc.TabStop = true;
            this.btnSalirYDesc.Text = "Salir y desconectarse";
            this.btnSalirYDesc.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnSalirYDesc_LinkClicked);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.GhostWhite;
            this.panel8.Location = new System.Drawing.Point(0, 336);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(5, 36);
            this.panel8.TabIndex = 50;
            // 
            // btnReportes
            // 
            this.btnReportes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnReportes.FlatAppearance.BorderSize = 0;
            this.btnReportes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportes.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnReportes.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnReportes.Location = new System.Drawing.Point(-3, 336);
            this.btnReportes.Name = "btnReportes";
            this.btnReportes.Size = new System.Drawing.Size(167, 36);
            this.btnReportes.TabIndex = 13;
            this.btnReportes.Text = "Reportes";
            this.btnReportes.UseVisualStyleBackColor = false;
            this.btnReportes.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.GhostWhite;
            this.panel7.Location = new System.Drawing.Point(0, 294);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(5, 36);
            this.panel7.TabIndex = 48;
            // 
            // btnCasosdePrueba
            // 
            this.btnCasosdePrueba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnCasosdePrueba.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnCasosdePrueba.FlatAppearance.BorderSize = 0;
            this.btnCasosdePrueba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCasosdePrueba.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnCasosdePrueba.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCasosdePrueba.Location = new System.Drawing.Point(-3, 294);
            this.btnCasosdePrueba.Name = "btnCasosdePrueba";
            this.btnCasosdePrueba.Size = new System.Drawing.Size(167, 36);
            this.btnCasosdePrueba.TabIndex = 12;
            this.btnCasosdePrueba.Text = "Casos de prueba";
            this.btnCasosdePrueba.UseVisualStyleBackColor = false;
            this.btnCasosdePrueba.Click += new System.EventHandler(this.btnCasosdePrueba_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.GhostWhite;
            this.panel6.Location = new System.Drawing.Point(0, 252);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(5, 36);
            this.panel6.TabIndex = 46;
            // 
            // btnPlanesdePrueba
            // 
            this.btnPlanesdePrueba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnPlanesdePrueba.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnPlanesdePrueba.FlatAppearance.BorderSize = 0;
            this.btnPlanesdePrueba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlanesdePrueba.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnPlanesdePrueba.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnPlanesdePrueba.Location = new System.Drawing.Point(-3, 252);
            this.btnPlanesdePrueba.Name = "btnPlanesdePrueba";
            this.btnPlanesdePrueba.Size = new System.Drawing.Size(167, 36);
            this.btnPlanesdePrueba.TabIndex = 11;
            this.btnPlanesdePrueba.Text = "Planes de prueba";
            this.btnPlanesdePrueba.UseVisualStyleBackColor = false;
            this.btnPlanesdePrueba.Click += new System.EventHandler(this.btnPlanesdePrueba_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.GhostWhite;
            this.panel5.Location = new System.Drawing.Point(0, 210);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(5, 36);
            this.panel5.TabIndex = 44;
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProductos.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnProductos.FlatAppearance.BorderSize = 0;
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnProductos.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnProductos.Location = new System.Drawing.Point(-3, 210);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(167, 36);
            this.btnProductos.TabIndex = 10;
            this.btnProductos.Text = "Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.GhostWhite;
            this.panel4.Location = new System.Drawing.Point(0, 168);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(5, 36);
            this.panel4.TabIndex = 42;
            // 
            // btnProyectos
            // 
            this.btnProyectos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnProyectos.FlatAppearance.BorderSize = 0;
            this.btnProyectos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProyectos.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnProyectos.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnProyectos.Location = new System.Drawing.Point(-3, 168);
            this.btnProyectos.Name = "btnProyectos";
            this.btnProyectos.Size = new System.Drawing.Size(167, 36);
            this.btnProyectos.TabIndex = 9;
            this.btnProyectos.Text = "Proyectos";
            this.btnProyectos.UseVisualStyleBackColor = false;
            this.btnProyectos.Click += new System.EventHandler(this.btnProyectos_Click);
            // 
            // lblUsuarioLogueado
            // 
            this.lblUsuarioLogueado.AutoSize = true;
            this.lblUsuarioLogueado.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuarioLogueado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUsuarioLogueado.Font = new System.Drawing.Font("Leelawadee UI", 9.25F);
            this.lblUsuarioLogueado.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblUsuarioLogueado.Location = new System.Drawing.Point(29, 119);
            this.lblUsuarioLogueado.Name = "lblUsuarioLogueado";
            this.lblUsuarioLogueado.Size = new System.Drawing.Size(35, 17);
            this.lblUsuarioLogueado.TabIndex = 28;
            this.lblUsuarioLogueado.Text = "User";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(48, 66);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(57, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 27;
            this.pictureBox6.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 29);
            this.panel2.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.label2.Location = new System.Drawing.Point(11, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Productos";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.panel3.Location = new System.Drawing.Point(157, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(583, 29);
            this.panel3.TabIndex = 27;
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(765, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(36, 29);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 11;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.btnMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Image")));
            this.btnMinimizar.Location = new System.Drawing.Point(728, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(40, 29);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 10;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // lblContadorRegistros
            // 
            this.lblContadorRegistros.AutoSize = true;
            this.lblContadorRegistros.Location = new System.Drawing.Point(614, 403);
            this.lblContadorRegistros.Name = "lblContadorRegistros";
            this.lblContadorRegistros.Size = new System.Drawing.Size(96, 13);
            this.lblContadorRegistros.TabIndex = 37;
            this.lblContadorRegistros.Text = "Total de productos";
            // 
            // dgvProductos
            // 
            this.dgvProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProductos.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductos.Location = new System.Drawing.Point(247, 240);
            this.dgvProductos.Name = "dgvProductos";
            this.dgvProductos.Size = new System.Drawing.Size(493, 150);
            this.dgvProductos.TabIndex = 6;
            this.dgvProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductos_CellClick);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLimpiar.Location = new System.Drawing.Point(551, 169);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 5;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnConsultar.Location = new System.Drawing.Point(366, 169);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 23);
            this.btnConsultar.TabIndex = 4;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // chkObtenerTodos
            // 
            this.chkObtenerTodos.AutoSize = true;
            this.chkObtenerTodos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkObtenerTodos.Location = new System.Drawing.Point(448, 92);
            this.chkObtenerTodos.Name = "chkObtenerTodos";
            this.chkObtenerTodos.Size = new System.Drawing.Size(62, 18);
            this.chkObtenerTodos.TabIndex = 2;
            this.chkObtenerTodos.Text = "Todos";
            this.chkObtenerTodos.UseVisualStyleBackColor = true;
            this.chkObtenerTodos.CheckedChanged += new System.EventHandler(this.chkSinBorrar_CheckedChanged);
            // 
            // chkDadosBaja
            // 
            this.chkDadosBaja.AutoSize = true;
            this.chkDadosBaja.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkDadosBaja.Location = new System.Drawing.Point(448, 115);
            this.chkDadosBaja.Name = "chkDadosBaja";
            this.chkDadosBaja.Size = new System.Drawing.Size(101, 18);
            this.chkDadosBaja.TabIndex = 3;
            this.chkDadosBaja.Text = "Dados de baja";
            this.chkDadosBaja.UseVisualStyleBackColor = true;
            this.chkDadosBaja.CheckedChanged += new System.EventHandler(this.chkTodos_CheckedChanged);
            // 
            // lblProducto
            // 
            this.lblProducto.AutoSize = true;
            this.lblProducto.Location = new System.Drawing.Point(335, 69);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(107, 13);
            this.lblProducto.TabIndex = 14;
            this.lblProducto.Text = "Nombre de producto:";
            // 
            // txtProductos
            // 
            this.txtProductos.Location = new System.Drawing.Point(448, 66);
            this.txtProductos.MaxLength = 50;
            this.txtProductos.Name = "txtProductos";
            this.txtProductos.Size = new System.Drawing.Size(178, 20);
            this.txtProductos.TabIndex = 1;
            this.txtProductos.TextChanged += new System.EventHandler(this.txtProductos_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(688, 435);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(308, 210);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(375, 10);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 40;
            this.pictureBox3.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(170, 472);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 12);
            this.label1.TabIndex = 53;
            this.label1.Text = "¿Necesitas ayuda?";
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackgroundImage = global::Proyecto_PAVI_2020.Properties.Resources.delete_remove_bin_icon_icons_com_72400;
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Location = new System.Drawing.Point(322, 404);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(40, 37);
            this.btnEliminar.TabIndex = 8;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click_1);
            this.btnEliminar.MouseHover += new System.EventHandler(this.btnEliminar_MouseHover_1);
            // 
            // btnActualizar
            // 
            this.btnActualizar.BackColor = System.Drawing.Color.Transparent;
            this.btnActualizar.BackgroundImage = global::Proyecto_PAVI_2020.Properties.Resources.edit_modify_icon_icons_com_72390;
            this.btnActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnActualizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnActualizar.FlatAppearance.BorderSize = 0;
            this.btnActualizar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnActualizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActualizar.ForeColor = System.Drawing.Color.Transparent;
            this.btnActualizar.Location = new System.Drawing.Point(288, 402);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(40, 40);
            this.btnActualizar.TabIndex = 7;
            this.btnActualizar.UseVisualStyleBackColor = false;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click_1);
            this.btnActualizar.MouseHover += new System.EventHandler(this.btnActualizar_MouseHover_1);
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackgroundImage = global::Proyecto_PAVI_2020.Properties.Resources.add;
            this.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAgregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAgregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Location = new System.Drawing.Point(263, 412);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(24, 24);
            this.btnAgregar.TabIndex = 6;
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click_1);
            this.btnAgregar.MouseHover += new System.EventHandler(this.btnAgregar_MouseHover_1);
            // 
            // FrmProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(800, 491);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblContadorRegistros);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.chkObtenerTodos);
            this.Controls.Add(this.txtProductos);
            this.Controls.Add(this.dgvProductos);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblProducto);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.chkDadosBaja);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnMinimizar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmProductos";
            this.Opacity = 0.97D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmProductos";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblUsuarioLogueado;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnCasosdePrueba;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnPlanesdePrueba;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnProyectos;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnReportes;
        private System.Windows.Forms.Label lblContadorRegistros;
        private System.Windows.Forms.DataGridView dgvProductos;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.CheckBox chkObtenerTodos;
        private System.Windows.Forms.CheckBox chkDadosBaja;
        private System.Windows.Forms.Label lblProducto;
        private System.Windows.Forms.TextBox txtProductos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.LinkLabel btnSalirYDesc;
        private System.Windows.Forms.LinkLabel btnVolver;
    }
}