﻿
using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using Proyecto_PAVI_2020.Negocio.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Presentacion
{
    public partial class FrmTransaccionCiclosPrueba : Form
    {
        private readonly PlanesDePruebaServicio _planesDePruebaServicio;
        private PlanesDePruebas _planPruebaSeleccionado;
        private readonly UsuarioServicio _usuarioServicio;
        private readonly CiclosDePruebaServicio _ciclosDePruebaServicio;
        private readonly CasosDePruebaServicio _casosDePruebaServicio;
        private CicloDePrueba _cicloDePruebaSeleccionado;
        private FormMode formMode = FormMode.Nuevo;
        public FrmTransaccionCiclosPrueba()
        {
            InitializeComponent();
            _planesDePruebaServicio = new PlanesDePruebaServicio();
            _casosDePruebaServicio = new CasosDePruebaServicio();
            _ciclosDePruebaServicio = new CiclosDePruebaServicio();
            _usuarioServicio = new UsuarioServicio();
        }

        private void LlenarCombo(ComboBox cbo, Object source, string display, String value)
        {
            cbo.DataSource = source;
            cbo.DisplayMember = display;
            cbo.ValueMember = value;
            cbo.SelectedIndex = -1;
        }

        public void InicializarFormulario(PlanesDePruebas planSeleccionado, FormMode op)
        {
            formMode = op;
            _planPruebaSeleccionado = planSeleccionado;

        }
        public void InicializarFormularioConCiclo(CicloDePrueba cicloSeleccionado, PlanesDePruebas planSeleccionado, FormMode opcion)
        {
            formMode = opcion;
            _cicloDePruebaSeleccionado = cicloSeleccionado;
            _planPruebaSeleccionado = planSeleccionado;

        }

        private void FrmTransaccionCiclosPrueba_Load(object sender, EventArgs e)
        {
            var usuarios = _usuarioServicio.ObtenerUsuarios();
            var casosPrueba = _casosDePruebaServicio.ObtenerTodos();
            var usuariosTester = _usuarioServicio.ObtenerUsuariosTester();
            var listPlan = new List<PlanesDePruebas>();
            listPlan.Add(_planPruebaSeleccionado);
            LlenarCombo(cmbPlanPrueba, listPlan, "Nombre", "IdPlanDePrueba");
            LlenarCombo(cmbResponsable, usuarios, "NombreUsuario", "IdUsuario");
            LlenarCombo(cmbTester, usuariosTester, "NombreUsuario", "IdUsuario");
            LlenarCombo(cmbCasoPrueba, casosPrueba, "Titulo", "IdCasoDePrueba");
            cmbPlanPrueba.SelectedValue = _planPruebaSeleccionado.IdPlanDePrueba;
            cmbPlanPrueba.Enabled = false;
            switch (formMode)
            {
                case FormMode.Nuevo:
                    {
                        lblTransaccion.Text = "Agregar ciclo de prueba/detalle";
                        break;
                    }
                case FormMode.Actualizar:
                    {
                        lblTransaccion.Text = "Actualizar ciclo de prueba/detalle";
                        MostrarDatos();
                        break;
                    }

                case FormMode.Eliminar:
                    {
                        MostrarDatos();
                        lblTransaccion.Text = "Eliminar ciclo de prueba/detalle";
                        cmbAceptadoDetalle.Enabled = false;
                        cmbAceptado.Enabled = false;
                        cmbCasoPrueba.Enabled = false;
                        cmbPlanPrueba.Enabled = false;
                        cmbResponsable.Enabled = false;
                        cmbTester.Enabled = false;
                        txtHoras.Enabled = false;
                        dtpFechaEjecucionDetalle.Enabled = false;
                        dtpFin.Enabled = false;
                        dtpInicio.Enabled = false;
                        break;
                    }
            }


        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rta == DialogResult.No)
                {
                    return false;
                }
                else
                {
                    Application.Exit();
                    return true;
                }

            }
            return base.ProcessDialogKey(keyData);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            switch (formMode)
            {
                case FormMode.Nuevo:
                    if (cmbResponsable.SelectedIndex != -1 && cmbPlanPrueba.SelectedIndex != -1 && cmbCasoPrueba.SelectedIndex != -1 && cmbTester.SelectedIndex != -1
                        && cmbAceptado.SelectedIndex != -1 && cmbAceptadoDetalle.SelectedIndex != -1 && !string.IsNullOrEmpty(txtHoras.Text) && dtpInicio.Value != null && dtpFin.Value != null
                        && dtpFechaEjecucionDetalle.Value != null)
                    {
                        var cicloPrueba = SetearCicloDePrueba(null, (int)cmbResponsable.SelectedValue, (int)cmbPlanPrueba.SelectedValue, dtpInicio.Value, dtpFin.Value, cmbAceptado.SelectedIndex);
                        var detalleCiclo = SetearDetalle(null, null, (int)cmbCasoPrueba.SelectedValue, dtpFechaEjecucionDetalle.Value, cmbAceptadoDetalle.SelectedIndex, txtHoras.Text, (int)cmbTester.SelectedValue);


                        var agregado = _ciclosDePruebaServicio.TransaccionAgregarCiclo(cicloPrueba, detalleCiclo);
                        if (agregado == true)
                        {
                            MessageBox.Show("Se ha agregado correctamente el ciclo de prueba.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("No se pudo agregar el ciclo de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Todos los campos son requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Actualizar:
                    if (cmbResponsable.SelectedIndex != -1 && cmbPlanPrueba.SelectedIndex != -1 && cmbCasoPrueba.SelectedIndex != -1 && cmbTester.SelectedIndex != -1
                        && cmbAceptado.SelectedIndex != -1 && cmbAceptadoDetalle.SelectedIndex != -1 && !string.IsNullOrEmpty(txtHoras.Text))
                    {

                        var cicloPrueba = SetearCicloDePrueba(_cicloDePruebaSeleccionado.IdCicloPrueba, (int)cmbResponsable.SelectedValue, (int)cmbPlanPrueba.SelectedValue, dtpInicio.Value, dtpFin.Value, cmbAceptado.SelectedIndex);

                        var detalleActualizar = _ciclosDePruebaServicio.BuscarDetallePorIdCiclo(_cicloDePruebaSeleccionado.IdCicloPrueba);
                        var detalleCiclo = SetearDetalle(detalleActualizar.IdDetalleCicloPrueba, _cicloDePruebaSeleccionado.IdCicloPrueba, (int)cmbCasoPrueba.SelectedValue, dtpFechaEjecucionDetalle.Value, cmbAceptadoDetalle.SelectedIndex, txtHoras.Text, (int)cmbTester.SelectedValue);
                        var modificado = _ciclosDePruebaServicio.TransaccionEditarCiclo(cicloPrueba, detalleCiclo);
                        if (modificado == true)
                        {
                            MessageBox.Show("Se ha actualizado correctamente el ciclo de prueba.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("No se pudo actualizar el ciclo de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Close();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Todos los campos son requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case FormMode.Eliminar:
                    var detalle = _ciclosDePruebaServicio.BuscarDetallePorIdCiclo(_cicloDePruebaSeleccionado.IdCicloPrueba);
                    var eliminado = _ciclosDePruebaServicio.TransaccionEliminarCiclo(_cicloDePruebaSeleccionado.IdCicloPrueba, detalle.IdDetalleCicloPrueba);
                    if (eliminado == true)
                    {
                        MessageBox.Show("Se ha eliminado correctamente el ciclo de prueba.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el caso de prueba.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    break;
                default:
                    break;
            }
        }

        private void txtHoras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private CicloDePrueba SetearCicloDePrueba(int? idCiclo, int idResponsable, int idPlan, DateTime fechaI, DateTime fechaF, int aceptado)
        {

            var cicloPrueba = new CicloDePrueba();
            cicloPrueba.IdCicloPrueba = idCiclo ?? 0;
            cicloPrueba.IdResponsable = idResponsable;
            cicloPrueba.IdPlanDePrueba = idPlan;
            cicloPrueba.FechaInicioEjecucion = fechaI;
            cicloPrueba.FechaFinEjecucion = fechaF;
            cicloPrueba.Aceptado = ConvertirAceptadoBool(aceptado);
            cicloPrueba.Borrado = false;
            return cicloPrueba;
        }
        private DetalleCicloDePrueba SetearDetalle(int? idDetalle, int? idCiclo, int idCaso, DateTime fechaE, int aceptado, string cantidadHoras, int idUsuario)
        {
            var detalleCiclo = new DetalleCicloDePrueba();
            detalleCiclo.IdDetalleCicloPrueba = idDetalle ?? 0;
            detalleCiclo.IdCicloDePrueba = idCiclo ?? 0;
            detalleCiclo.IdCasoDePrueba = idCaso;
            detalleCiclo.FechaEjecucion = fechaE;
            detalleCiclo.Aceptado = ConvertirAceptadoBool(aceptado);
            detalleCiclo.CantidadDeHoras = Convert.ToDecimal(cantidadHoras);
            detalleCiclo.IdUsuarioTester = idUsuario;
            detalleCiclo.Borrado = false;
            return detalleCiclo;
        }

        public bool ConvertirAceptadoBool(int index)
        {
            var aceptadoBoolDetalle = false;
            if (index == 0)
            {
                aceptadoBoolDetalle = true;

            }
            else
            {
                aceptadoBoolDetalle = false;
            }
            return aceptadoBoolDetalle;
        }

        public int ConvertirAceptadoInt(bool aceptado)
        {
            var aceptadoInt = 0;
            if (aceptado)
            {
                aceptadoInt = 0;

            }
            else
            {
                aceptadoInt = 1;
            }
            return aceptadoInt;
        }

        private void MostrarDatos()
        {
            if (_cicloDePruebaSeleccionado != null)
            {
                var detalle = _ciclosDePruebaServicio.BuscarDetallePorIdCiclo(_cicloDePruebaSeleccionado.IdCicloPrueba);
                cmbResponsable.SelectedValue = _cicloDePruebaSeleccionado.IdResponsable;
                cmbPlanPrueba.SelectedValue = _cicloDePruebaSeleccionado.IdPlanDePrueba;
                dtpInicio.Value = (DateTime)_cicloDePruebaSeleccionado.FechaInicioEjecucion;
                dtpFin.Value = (DateTime)_cicloDePruebaSeleccionado.FechaFinEjecucion;
                cmbAceptado.SelectedIndex = ConvertirAceptadoInt((bool)_cicloDePruebaSeleccionado.Aceptado);
                cmbCasoPrueba.SelectedValue = detalle.IdCasoDePrueba;
                cmbTester.SelectedValue = detalle.IdUsuarioTester;
                txtHoras.Text = detalle.CantidadDeHoras.ToString();
                dtpFechaEjecucionDetalle.Value = (DateTime)detalle.FechaEjecucion;
                cmbAceptadoDetalle.SelectedIndex = ConvertirAceptadoInt((bool)detalle.Aceptado);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            var rta = MessageBox.Show("Seguro que desea salir?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rta == DialogResult.No)
            {
                return;
            }
            else
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
