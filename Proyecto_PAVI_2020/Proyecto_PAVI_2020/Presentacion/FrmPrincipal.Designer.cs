﻿namespace Proyecto_PAVI_2020.Presentacion
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.panel3 = new System.Windows.Forms.Panel();
            this.btncerrar = new System.Windows.Forms.PictureBox();
            this.btnminimizar = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblUsuarioLogueado = new System.Windows.Forms.Label();
            this.btnProyectos = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnProductos = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnPlanesdePrueba = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnReportes = new System.Windows.Forms.Button();
            this.btnCasosdePrueba = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnSalirYDesc = new System.Windows.Forms.LinkLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btncerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnminimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.panel3.Location = new System.Drawing.Point(157, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(583, 29);
            this.panel3.TabIndex = 26;
            // 
            // btncerrar
            // 
            this.btncerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.btncerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btncerrar.Image = ((System.Drawing.Image)(resources.GetObject("btncerrar.Image")));
            this.btncerrar.Location = new System.Drawing.Point(765, 0);
            this.btncerrar.Name = "btncerrar";
            this.btncerrar.Size = new System.Drawing.Size(36, 29);
            this.btncerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btncerrar.TabIndex = 24;
            this.btncerrar.TabStop = false;
            this.btncerrar.Click += new System.EventHandler(this.btncerrar_Click);
            // 
            // btnminimizar
            // 
            this.btnminimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.btnminimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnminimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnminimizar.Image")));
            this.btnminimizar.Location = new System.Drawing.Point(728, 0);
            this.btnminimizar.Name = "btnminimizar";
            this.btnminimizar.Size = new System.Drawing.Size(40, 29);
            this.btnminimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnminimizar.TabIndex = 25;
            this.btnminimizar.TabStop = false;
            this.btnminimizar.Click += new System.EventHandler(this.btnminimizar_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(401, 159);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(166, 87);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 39;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(170, 472);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "¿Necesitas ayuda?";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(144)))), ((int)(((byte)(144)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 29);
            this.panel2.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(11, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Menu Principal";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(48, 66);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(57, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 49;
            this.pictureBox6.TabStop = false;
            // 
            // lblUsuarioLogueado
            // 
            this.lblUsuarioLogueado.AutoSize = true;
            this.lblUsuarioLogueado.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuarioLogueado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUsuarioLogueado.Font = new System.Drawing.Font("Leelawadee UI", 9.25F);
            this.lblUsuarioLogueado.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblUsuarioLogueado.Location = new System.Drawing.Point(29, 119);
            this.lblUsuarioLogueado.Name = "lblUsuarioLogueado";
            this.lblUsuarioLogueado.Size = new System.Drawing.Size(35, 17);
            this.lblUsuarioLogueado.TabIndex = 50;
            this.lblUsuarioLogueado.Text = "User";
            // 
            // btnProyectos
            // 
            this.btnProyectos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnProyectos.FlatAppearance.BorderSize = 0;
            this.btnProyectos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProyectos.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnProyectos.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnProyectos.Location = new System.Drawing.Point(-3, 168);
            this.btnProyectos.Name = "btnProyectos";
            this.btnProyectos.Size = new System.Drawing.Size(167, 36);
            this.btnProyectos.TabIndex = 1;
            this.btnProyectos.Text = "Proyectos";
            this.btnProyectos.UseVisualStyleBackColor = false;
            this.btnProyectos.Click += new System.EventHandler(this.btnProyectos_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.GhostWhite;
            this.panel4.Location = new System.Drawing.Point(0, 168);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(5, 36);
            this.panel4.TabIndex = 52;
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnProductos.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnProductos.FlatAppearance.BorderSize = 0;
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnProductos.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnProductos.Location = new System.Drawing.Point(-3, 210);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(167, 36);
            this.btnProductos.TabIndex = 2;
            this.btnProductos.Text = "Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.GhostWhite;
            this.panel5.Location = new System.Drawing.Point(0, 210);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(5, 36);
            this.panel5.TabIndex = 54;
            // 
            // btnPlanesdePrueba
            // 
            this.btnPlanesdePrueba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnPlanesdePrueba.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnPlanesdePrueba.FlatAppearance.BorderSize = 0;
            this.btnPlanesdePrueba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlanesdePrueba.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnPlanesdePrueba.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnPlanesdePrueba.Location = new System.Drawing.Point(-3, 252);
            this.btnPlanesdePrueba.Name = "btnPlanesdePrueba";
            this.btnPlanesdePrueba.Size = new System.Drawing.Size(167, 36);
            this.btnPlanesdePrueba.TabIndex = 3;
            this.btnPlanesdePrueba.Text = "Planes de prueba";
            this.btnPlanesdePrueba.UseVisualStyleBackColor = false;
            this.btnPlanesdePrueba.Click += new System.EventHandler(this.btnPlanesdePrueba_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.GhostWhite;
            this.panel6.Location = new System.Drawing.Point(0, 252);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(5, 36);
            this.panel6.TabIndex = 56;
            // 
            // btnReportes
            // 
            this.btnReportes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnReportes.FlatAppearance.BorderSize = 0;
            this.btnReportes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReportes.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnReportes.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnReportes.Location = new System.Drawing.Point(-3, 336);
            this.btnReportes.Name = "btnReportes";
            this.btnReportes.Size = new System.Drawing.Size(167, 36);
            this.btnReportes.TabIndex = 6;
            this.btnReportes.Text = "Reportes";
            this.btnReportes.UseVisualStyleBackColor = false;
            this.btnReportes.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // btnCasosdePrueba
            // 
            this.btnCasosdePrueba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.btnCasosdePrueba.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnCasosdePrueba.FlatAppearance.BorderSize = 0;
            this.btnCasosdePrueba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCasosdePrueba.Font = new System.Drawing.Font("Microsoft New Tai Lue", 9.75F);
            this.btnCasosdePrueba.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCasosdePrueba.Location = new System.Drawing.Point(-3, 294);
            this.btnCasosdePrueba.Name = "btnCasosdePrueba";
            this.btnCasosdePrueba.Size = new System.Drawing.Size(167, 36);
            this.btnCasosdePrueba.TabIndex = 4;
            this.btnCasosdePrueba.Text = "Casos de prueba";
            this.btnCasosdePrueba.UseVisualStyleBackColor = false;
            this.btnCasosdePrueba.Click += new System.EventHandler(this.btnCasosdePrueba_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.GhostWhite;
            this.panel7.Location = new System.Drawing.Point(0, 294);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(5, 36);
            this.panel7.TabIndex = 58;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.GhostWhite;
            this.panel8.Location = new System.Drawing.Point(0, 336);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(5, 36);
            this.panel8.TabIndex = 60;
            // 
            // btnSalirYDesc
            // 
            this.btnSalirYDesc.ActiveLinkColor = System.Drawing.SystemColors.ControlDark;
            this.btnSalirYDesc.AutoSize = true;
            this.btnSalirYDesc.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnSalirYDesc.LinkColor = System.Drawing.SystemColors.ControlDark;
            this.btnSalirYDesc.Location = new System.Drawing.Point(12, 451);
            this.btnSalirYDesc.Name = "btnSalirYDesc";
            this.btnSalirYDesc.Size = new System.Drawing.Size(108, 13);
            this.btnSalirYDesc.TabIndex = 91;
            this.btnSalirYDesc.TabStop = true;
            this.btnSalirYDesc.Text = "Salir y desconectarse";
            this.btnSalirYDesc.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnSalirYDesc_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.panel1.Controls.Add(this.btnSalirYDesc);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.btnCasosdePrueba);
            this.panel1.Controls.Add(this.btnReportes);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.btnPlanesdePrueba);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.btnProductos);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.btnProyectos);
            this.panel1.Controls.Add(this.lblUsuarioLogueado);
            this.panel1.Controls.Add(this.pictureBox6);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(164, 491);
            this.panel1.TabIndex = 0;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(800, 491);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btncerrar);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnminimizar);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPrincipal";
            this.Opacity = 0.97D;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.btncerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnminimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox btncerrar;
        private System.Windows.Forms.PictureBox btnminimizar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblUsuarioLogueado;
        private System.Windows.Forms.Button btnProyectos;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnPlanesdePrueba;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnReportes;
        private System.Windows.Forms.Button btnCasosdePrueba;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.LinkLabel btnSalirYDesc;
        private System.Windows.Forms.Panel panel1;
    }
}

