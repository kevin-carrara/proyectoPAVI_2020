﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Enumeraciones
{
    public enum PerfilEnum
    {
        Administrador = 1,
        Tester,
        Desarrollador,
        ResponsableReportes

    }
}
