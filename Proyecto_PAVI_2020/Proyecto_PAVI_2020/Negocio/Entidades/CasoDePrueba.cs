﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Entidades
{
    public class CasoDePrueba
    {
        public int IdCasoDePrueba { get; set; }
        public int IdPlanDePrueba { get; set; }
        public string NombrePlanPrueba { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public int? IdResponsable { get; set; }
        public string NombreResponsable { get; set; }
        public bool? Borrado{ get; set; }
    }
}
