﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Entidades
{
    public class PlanesDePruebas
    {
        public int IdPlanDePrueba { get; set; }
        public int IdProyecto { get; set; }
        public string DescripcionProyecto { get; set; }
        public string Nombre { get; set; }
        public int? IdResponsable { get; set; }
        public string NombreResponsable { get; set; }
        public string Descripcion { get; set; }
        public bool? Borrado { get; set; }
    }
}
