﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Entidades
{
    public class Proyecto
    {
        public int IdProyecto { get; set; }
        public int? IdResponsable { get; set; }
        public string NombreResponsable { get; set; }
        public string Descripcion { get; set; }
        public string Version { get; set; }
        public string Alcance { get; set; }
        public int? IdProducto{ get; set; }
        public string NombreProducto { get; set; }
        public bool? Borrado { get; set; }
    }
}
