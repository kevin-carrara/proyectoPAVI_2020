﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Entidades
{
    public class Producto
    {
        public int IdProducto { get; set; } 
        public string NombreProducto { get; set; }
        public bool Borrado { get; set; }
    }
}
