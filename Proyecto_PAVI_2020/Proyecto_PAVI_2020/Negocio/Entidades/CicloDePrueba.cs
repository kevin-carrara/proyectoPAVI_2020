﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Entidades
{
    public class CicloDePrueba
    {
        public int IdCicloPrueba { get; set; }
        public DateTime? FechaInicioEjecucion { get; set; }
        public DateTime? FechaFinEjecucion { get; set; }
        public int? IdResponsable { get; set; }
        public string NombreResponsable { get; set; }
        public int? IdPlanDePrueba { get; set; }
        public string NombrePlanPrueba { get; set; }
        public bool? Aceptado { get; set; }
        public bool? Borrado { get; set; }
    }
}
