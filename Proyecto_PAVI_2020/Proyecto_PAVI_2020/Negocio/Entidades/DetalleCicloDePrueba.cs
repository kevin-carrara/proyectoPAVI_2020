﻿using System;

namespace Proyecto_PAVI_2020.Negocio.Entidades
{
    public class DetalleCicloDePrueba
    {
        public int IdDetalleCicloPrueba { get; set; }
        public int? IdCicloDePrueba { get; set; }
        public int? IdCasoDePrueba { get; set; }
        public int? IdUsuarioTester { get; set; }
        public decimal? CantidadDeHoras { get; set; }
        public DateTime? FechaEjecucion { get; set; }
        public bool? Aceptado { get; set; }
        public bool? Borrado { get; set; }
    }
}
