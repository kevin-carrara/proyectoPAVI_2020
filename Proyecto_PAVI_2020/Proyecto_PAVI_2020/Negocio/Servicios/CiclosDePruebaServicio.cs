﻿using Proyecto_PAVI_2020.Datos.Dao;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Servicios
{
    public class CiclosDePruebaServicio
    {
        private readonly CicloDePruebaDao _cicloDePruebaDao;

        public CiclosDePruebaServicio()
        {
            _cicloDePruebaDao = new CicloDePruebaDao();
        }
        public DetalleCicloDePrueba BuscarDetallePorIdCiclo(int id)
        {
            var detalle = _cicloDePruebaDao.BuscarDetallePorIdCiclo(id);
            return detalle;
        }
        public bool TransaccionAgregarCiclo(CicloDePrueba cicloDePrueba, DetalleCicloDePrueba detalle)
        {
           var agregado =  _cicloDePruebaDao.TransaccionAgregarCiclo(cicloDePrueba, detalle);
            return agregado;
        }

        public bool TransaccionEliminarCiclo(int idCicloDePrueba, int idDetalle)
        {
            var eliminado =_cicloDePruebaDao.TransaccionEliminarCiclo(idCicloDePrueba, idDetalle);
            return eliminado;
        }



        public bool TransaccionEditarCiclo(CicloDePrueba cicloEntidad, DetalleCicloDePrueba detalle)
        {
            var actualizado = _cicloDePruebaDao.TransaccionEditarCiclo(cicloEntidad, detalle);
            return actualizado;
        }

        public IList<CicloDePrueba> ObtenerTodos(int idPlanPrueba)
        {
            var ciclosDePrueba = _cicloDePruebaDao.ObtenerTodos(idPlanPrueba);
            return ciclosDePrueba;
        }

        public IList<CicloDePrueba> ObtenerDadosBaja (int idPlanPrueba)
        {
            var ciclosDadosBaja = _cicloDePruebaDao.ObtenerDadosBaja(idPlanPrueba);
            return ciclosDadosBaja;
        }

        public IList<CicloDePrueba> BuscarConFiltros( int? idResponsable, int idPlanSeleccionado)
        {
            var ciclosPrueba = _cicloDePruebaDao.BuscarConFiltros( idResponsable, idPlanSeleccionado);
            return ciclosPrueba;
        }
    }
}
