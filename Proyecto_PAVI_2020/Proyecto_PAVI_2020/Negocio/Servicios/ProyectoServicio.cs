﻿using Proyecto_PAVI_2020.Datos.Dao;
using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Servicios
{
    
    class ProyectoServicio
    {
        private readonly ProyectosDao _proyectosDao;
        public ProyectoServicio()
        {
            _proyectosDao = new ProyectosDao();
        }

        public IList<Proyecto> ObtenerProyectos()
        {
            var listaProyectos = _proyectosDao.ObtenerProyectos();

            return listaProyectos;
        }
        public IList<Proyecto> ObtenerDadosDeBaja()
        {
            var lista = _proyectosDao.ObtenerDadosDeBaja();

            return lista;
        }
        public bool AgregarProyecto(Proyecto proyectoEntidad)
        {
            var Agregados = _proyectosDao.AgregarProyecto(proyectoEntidad);

            return Agregados;
        }

        public bool EliminarProyecto(Proyecto ProyectoEntidad)
        {
            var Eliminado = _proyectosDao.EliminarProyecto(ProyectoEntidad);
            return Eliminado;
        }
        public bool ActualizarProyecto(Proyecto proyectoEntidad)
        {
            var actualizado = _proyectosDao.ActualizarProyecto(proyectoEntidad);
            return actualizado;
        }
        
        public IList<Proyecto> BuscarPorFiltro(string des,int? idRes,int? idProdu)
        {
            var lista = _proyectosDao.BuscarPorFiltro( des,  idRes, idProdu);
            return lista;
        }
        public bool ExisteProyecto(Proyecto proyecto)
        {
            var existeProyecto= _proyectosDao.ExisteProyecto(proyecto);
            return existeProyecto;
        }

        public Proyecto BuscarPorId(int id)
        {
            var proyecto = _proyectosDao.BuscarPorId(id);
            return proyecto;
        }

       
    }
}

