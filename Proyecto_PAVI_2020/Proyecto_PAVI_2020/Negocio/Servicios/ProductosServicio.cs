﻿using Proyecto_PAVI_2020.Datos.Dao;
using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_PAVI_2020.Negocio.Servicios
{
    class ProductosServicio
    {   
        private readonly ProductosDao _productosDao;

        public ProductosServicio()
        {
            _productosDao = new ProductosDao();
        }

        public Producto BuscarPorId(int idProducto)
        {
            var product = _productosDao.BuscarPorId(idProducto);

            return product;
        }
        public IList<Producto> ObtenerProductos()
        {  
            var listaProductos = _productosDao.ObtenerProductos();

            return listaProductos;
        }
        public IList<Producto> ObtenerDadosDeBaja()
        {
            var listaProductos = _productosDao.ObtenerDadosDeBaja();

            return listaProductos;
        }

        public IList<Producto> BuscarPorNombre(string nombre)
        {
            var listaProductos = _productosDao.BuscarPorNombre(nombre);
            return listaProductos;
        }

        public bool AgregarProductos(Producto productoEntidad)
        {
            var Agregados = _productosDao.AgregarProductos(productoEntidad);

            return Agregados;
        }
        public bool EliminarProductos(Producto productoEntidad)
        {
            var Eliminado = _productosDao.EliminarProductos(productoEntidad);
            return Eliminado;
        }
        public bool ActualizarProductos(Producto productoEntidad)
        {
            var actualizado = _productosDao.ActualizarProductos(productoEntidad);
            return actualizado;
        }

        public bool ExisteProductoEnDb(string nombre)
        {
            var existeProducto = _productosDao.ExisteProductoEnBd(nombre);
            return existeProducto;
        }

    }
}
       
       

     
    

