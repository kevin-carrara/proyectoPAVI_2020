﻿using Proyecto_PAVI_2020.Datos.Dao;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Servicios
{
    public class UsuarioServicio
    {
        private readonly UsuarioDao _usuarioDao;

        public UsuarioServicio()
        {
            _usuarioDao = new UsuarioDao();
        }

        public Usuario ValidarUsuarioYContraseña(string usuario, string contraseña)
        {
            var user = _usuarioDao.ValidarUsuarioYcontraseña(usuario, contraseña);
            return user;

        }
        public bool RecuperarContraseña(string nombreUsuario, string contraseñaNueva)
        {
            var contra = _usuarioDao.RecuperarContraseña(nombreUsuario, contraseñaNueva);
            return contra;
        }
        public IList<Usuario> ObtenerUsuarios()
        {
            var usuarios = _usuarioDao.ObtenerUsuarios();

            return usuarios;
        }

        public IList<Usuario> ObtenerUsuariosTester()
        {
            var usuariosTesters = _usuarioDao.ObtenerUsuarioTester();
            return usuariosTesters;
        }
    }
}
