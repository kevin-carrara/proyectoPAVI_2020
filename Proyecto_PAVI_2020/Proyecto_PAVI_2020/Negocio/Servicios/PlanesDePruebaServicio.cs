﻿using Proyecto_PAVI_2020.Datos.Dao;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Servicios
{
    public class PlanesDePruebaServicio
    {
        private readonly PlanesDePruebaDao _planesDePruebaDao;

        public PlanesDePruebaServicio()
        {
            _planesDePruebaDao = new PlanesDePruebaDao();
        }
        public IList<PlanesDePruebas> ObtenerTodos()
        {
            var planesDePrueba = _planesDePruebaDao.ObtenerPlanes();

            return planesDePrueba;
        }
        public PlanesDePruebas BuscarPorId(int idPlan)
        {
            var plan = _planesDePruebaDao.BuscarPorId(idPlan);
            return plan;
        }

        public IList<PlanesDePruebas> ObtenerDadosDeBaja()
        {
            var planesDePruebaBorrados = _planesDePruebaDao.ObtenerDadosDeBaja();

            return planesDePruebaBorrados;
        }

        public bool ExistePPruebaEnDb(PlanesDePruebas planPrueba)
        {
            var existePPrueba = _planesDePruebaDao.ExistePlanDePruebaEnDb(planPrueba);
            return existePPrueba;
        }

        public bool AgregarPlanDePrueba(PlanesDePruebas nuevoPlan)
        {
            var agregado = _planesDePruebaDao.AgregarPlan(nuevoPlan);
            return agregado;
        }

        public bool ActualizarPlanDePrueba(PlanesDePruebas planDePrueba)
        {
            var actualizado = _planesDePruebaDao.ActualizarPlan(planDePrueba);
            return actualizado;
        }

        public bool EliminarPlanDePrueba(int idPlanDePrueba)
        {
            var eliminado = _planesDePruebaDao.EliminarPlan(idPlanDePrueba);
            return eliminado;
        }

        public IList<PlanesDePruebas> BuscarConFiltros(string nombre, int? idResponsable, int? idProyecto)
        {
            var planesDePrueba = _planesDePruebaDao.BuscarConFiltros(nombre, idResponsable, idProyecto);
            return planesDePrueba;
        }
    }
}
