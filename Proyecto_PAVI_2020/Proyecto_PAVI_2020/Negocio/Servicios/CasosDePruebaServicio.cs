﻿using Proyecto_PAVI_2020.Datos.Dao;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Negocio.Servicios
{
    public class CasosDePruebaServicio
    {
        private readonly CasosDePruebaDao _casosDePruebaDao;

        public CasosDePruebaServicio()
        {
            _casosDePruebaDao = new CasosDePruebaDao();
        }

        public IList<CasoDePrueba> ObtenerTodos()
        {
            var casosPrueba = _casosDePruebaDao.ObtenerCasosDePrueba();
            return casosPrueba;
        }

        public IList<CasoDePrueba> ObtenerBorrados()
        {
            var casosPrueba = _casosDePruebaDao.ObtenerCasosDePruebaBorrados();
            return casosPrueba;
        }

        public IList<CasoDePrueba> ObtenerPorFiltro(string titulo,int? idResp,int? idPlan)
        {
            var casosPrueba = _casosDePruebaDao.ObtenerPorFiltros(titulo, idResp, idPlan);
            return casosPrueba;
        }
        public bool ActualizarCasoPrueba(CasoDePrueba casoDePrueba)
        {
            var actualizado = _casosDePruebaDao.ActualizarCasoDePrueba(casoDePrueba);
            return actualizado;
        }

        public bool EliminarCasoDePrueba(int idCasoPrueba)
        {
            var eliminado = _casosDePruebaDao.EliminarCasoDePrueba(idCasoPrueba);
            return eliminado;
        }

        public bool AgregarCasoDePrueba(CasoDePrueba casoPrueba)
        {
            var agregado = _casosDePruebaDao.AgregarCasoDePrueba(casoPrueba);
            return agregado;
        }
        public bool ExisteCasoDePruebaPorNombre(CasoDePrueba caso)
        {
            var existe = _casosDePruebaDao.ExisteCasoDePruebaPorNombre(caso);
            return existe;
        }
    }
}
