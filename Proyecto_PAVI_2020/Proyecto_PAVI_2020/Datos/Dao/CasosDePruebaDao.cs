﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System.Collections.Generic;
using System.Linq;

namespace Proyecto_PAVI_2020.Datos.Dao
{
    public class CasosDePruebaDao
    {
        private readonly BugTrackerEntities _bugTrackerContext;

        public CasosDePruebaDao()
        {
            _bugTrackerContext = new BugTrackerEntities();
        }
        
        public IList<CasoDePrueba> ObtenerCasosDePrueba()
        {
           
            var listaCasoPruebaEntidad = (from cp in _bugTrackerContext.CasosDePrueba
                                     join pp in _bugTrackerContext.PlanesDePrueba on cp.id_plan_prueba equals pp.id_plan_prueba
                                     join u in _bugTrackerContext.Usuarios on cp.id_responsable equals u.id_usuario
                                     where cp.borrado == false
                                     select new CasoDePrueba
                                     {
                                         IdCasoDePrueba = cp.id_caso_prueba,
                                         IdPlanDePrueba = pp.id_plan_prueba,
                                         NombrePlanPrueba = pp.nombre,
                                         Titulo = cp.titulo,
                                         Descripcion = cp.descripcion,
                                         IdResponsable = u.id_usuario,
                                         NombreResponsable = u.usuario,
                                         Borrado = cp.borrado
                                     }).ToList();
          
           
            return listaCasoPruebaEntidad;

        }
        public IList<CasoDePrueba> ObtenerPorFiltros(string titulo ,int? idResponsable,int? idPlan)
        {
            var listaCasoPruebaEntidad = (from cp in _bugTrackerContext.CasosDePrueba
                                          join pp in _bugTrackerContext.PlanesDePrueba on cp.id_plan_prueba equals pp.id_plan_prueba
                                          join u in _bugTrackerContext.Usuarios on cp.id_responsable equals u.id_usuario into user
                                          from us in user.DefaultIfEmpty()
                                          where cp.borrado == false
                                          &&(string.IsNullOrEmpty(cp.titulo) || cp.titulo.ToLower().Contains(titulo.ToLower()))
                                          && (idResponsable == null || cp.id_responsable == idResponsable)
                                          && (idPlan == null || cp.id_plan_prueba == idPlan)
                                          || ((idPlan == null || cp.id_plan_prueba == idPlan) && (idResponsable == null || cp.id_responsable == idResponsable) || (string.IsNullOrEmpty(cp.titulo) || cp.titulo.ToLower().Contains(titulo.ToLower())))
                                          select new CasoDePrueba
                                          {
                                              IdCasoDePrueba = cp.id_caso_prueba,
                                              IdPlanDePrueba = pp.id_plan_prueba,
                                              NombrePlanPrueba = pp.nombre,
                                              Titulo = cp.titulo,
                                              Descripcion = cp.descripcion,
                                              IdResponsable = us.id_usuario,
                                              NombreResponsable = us.usuario,
                                              Borrado = cp.borrado
                                          }).ToList();

            return listaCasoPruebaEntidad;


        }

        public IList<CasoDePrueba> ObtenerCasosDePruebaBorrados()
        {

            var listaCasoPruebaEntidad = (from cp in _bugTrackerContext.CasosDePrueba
                                          join pp in _bugTrackerContext.PlanesDePrueba on cp.id_plan_prueba equals pp.id_plan_prueba
                                          join u in _bugTrackerContext.Usuarios on cp.id_responsable equals u.id_usuario
                                          where cp.borrado == true

                                          select new CasoDePrueba
                                          {
                                              IdCasoDePrueba = cp.id_caso_prueba,
                                              IdPlanDePrueba = pp.id_plan_prueba,
                                              NombrePlanPrueba = pp.nombre,
                                              Titulo = cp.titulo,
                                              Descripcion = cp.descripcion,
                                              IdResponsable = u.id_usuario,
                                              NombreResponsable = u.usuario,
                                              Borrado = cp.borrado
                                          }).ToList();
           
            return listaCasoPruebaEntidad;

        }

        public IList<CasoDePrueba> ObtenerPorNombre(string nombre)
        {
            var listaCasosDePruebaEntidad = new List<CasoDePrueba>();
            var casosDePruebaEnDb = (from cp in _bugTrackerContext.CasosDePrueba
                                     join pp in _bugTrackerContext.PlanesDePrueba on cp.id_plan_prueba equals pp.id_plan_prueba
                                     join u in _bugTrackerContext.Usuarios on cp.id_responsable equals u.id_usuario
                                     where cp.borrado == false && cp.titulo.ToLower().StartsWith(nombre.ToLower())
                                     select cp).ToList();

            foreach (var casoEnDb in casosDePruebaEnDb)
            {
                var casoDePruebaEntidad = new CasoDePrueba();
                MapperBdAEntidad(casoEnDb, casoDePruebaEntidad);
                listaCasosDePruebaEntidad.Add(casoDePruebaEntidad);

            }

            return listaCasosDePruebaEntidad;
        }

        public bool ActualizarCasoDePrueba(CasoDePrueba casoDePruebaEntidad)
        {
            var casoDePruebaEnDb = (from cp in _bugTrackerContext.CasosDePrueba
                                    where cp.id_caso_prueba == casoDePruebaEntidad.IdCasoDePrueba
                                    && cp.borrado == false
                                    select cp).FirstOrDefault();
            MapperEntidadABd(casoDePruebaEntidad, casoDePruebaEnDb);

            return _bugTrackerContext.SaveChanges() > 0;
        }

        public bool EliminarCasoDePrueba(int idCasoPrueba)
        {
            var casoDePruebaEnDb = (from cp in _bugTrackerContext.CasosDePrueba
                                    where cp.id_caso_prueba == idCasoPrueba
                                    && cp.borrado == false
                                    select cp).FirstOrDefault();
            casoDePruebaEnDb.borrado = true;

            return _bugTrackerContext.SaveChanges() > 0;
        }

        public bool AgregarCasoDePrueba(CasoDePrueba casoDePruebaEntidad)
        {
            var casoDePruebaNuevoDb = new CasosDePrueba();
            var casoPorAñadir = MapperEntidadABd(casoDePruebaEntidad, casoDePruebaNuevoDb);
            _bugTrackerContext.CasosDePrueba.Add(casoPorAñadir);

            return _bugTrackerContext.SaveChanges() > 0;
        }

        public bool ExisteCasoDePruebaPorNombre(CasoDePrueba casoDePrueba)
        {
            var casoEnDb = (from cp in _bugTrackerContext.CasosDePrueba
                            where cp.borrado == false
                            && cp.titulo == casoDePrueba.Titulo 
                            && cp.descripcion == casoDePrueba.Descripcion
                            && cp.id_plan_prueba == casoDePrueba.IdPlanDePrueba
                            && cp.id_responsable == casoDePrueba.IdResponsable
                            select cp).FirstOrDefault();
            if (casoEnDb == null)
                return false;

            return true;
        }

        public CasoDePrueba MapperBdAEntidad(CasosDePrueba casosDePruebaBd, CasoDePrueba casosDePruebaEntidad)
        {
            if (casosDePruebaBd != null)
            {
                casosDePruebaEntidad.IdCasoDePrueba = casosDePruebaBd.id_caso_prueba;
                casosDePruebaEntidad.IdPlanDePrueba = casosDePruebaBd.id_plan_prueba;
                casosDePruebaEntidad.Titulo = casosDePruebaBd.titulo;
                casosDePruebaEntidad.Descripcion = casosDePruebaBd.descripcion;
                casosDePruebaEntidad.IdResponsable = (int)casosDePruebaBd.id_responsable;
                casosDePruebaEntidad.Borrado = (bool)casosDePruebaBd.borrado;
                return casosDePruebaEntidad;
            }
            return null;

        }

        public CasosDePrueba MapperEntidadABd(CasoDePrueba casosDePruebaEntidad, CasosDePrueba casosDePruebaBd)
        {

            casosDePruebaBd.id_caso_prueba = casosDePruebaEntidad.IdCasoDePrueba;
            casosDePruebaBd.id_plan_prueba = casosDePruebaEntidad.IdPlanDePrueba;
            casosDePruebaBd.titulo = casosDePruebaEntidad.Titulo;
            casosDePruebaBd.descripcion = casosDePruebaEntidad.Descripcion;
            casosDePruebaBd.id_responsable = casosDePruebaEntidad.IdResponsable;
            casosDePruebaBd.borrado = casosDePruebaEntidad.Borrado;
            return casosDePruebaBd;
        }
    }
}
