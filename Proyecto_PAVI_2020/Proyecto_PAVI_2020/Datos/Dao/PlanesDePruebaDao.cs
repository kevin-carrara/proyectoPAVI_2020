﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Datos.Dao
{
    public class PlanesDePruebaDao

    {
        private readonly BugTrackerEntities _bugTrackerContext;

        public PlanesDePruebaDao()
        {
            _bugTrackerContext = new BugTrackerEntities();
        }
        public PlanesDePruebas BuscarPorId(int id)
        {
            var planEntidad = new PlanesDePruebas();
            var planBd = (from p in _bugTrackerContext.PlanesDePrueba
                              where p.borrado == false && p.id_plan_prueba == id
                              select p).FirstOrDefault();
            return MapperBDAEntidad(planBd, planEntidad);
        }
        public IList<PlanesDePruebas> ObtenerPlanes()
        {
            var listaPlanes = (from pl in _bugTrackerContext.PlanesDePrueba
                          join pr in _bugTrackerContext.Proyectos on pl.id_proyecto equals pr.id_proyecto
                          join u in _bugTrackerContext.Usuarios on pl.id_responsable equals u.id_usuario into user
                          from us in user.DefaultIfEmpty()
                          where pl.borrado == false
                          select new PlanesDePruebas 
                          {
                              IdPlanDePrueba = pl.id_plan_prueba,
                              Nombre = pl.nombre,
                              Descripcion = pl.descripcion,
                              IdResponsable = us.id_usuario,
                              NombreResponsable = us.usuario,
                              IdProyecto = pr.id_proyecto,
                              DescripcionProyecto = pr.descripcion,
                              Borrado = pl.borrado

                          }).ToList();

            return listaPlanes;
        }

        public bool ExistePlanDePruebaEnDb(PlanesDePruebas planPrueba)
        {

            var productoEnDb = (from p in _bugTrackerContext.PlanesDePrueba
                                where p.borrado == false 
                                && p.nombre == planPrueba.Nombre
                                && p.id_proyecto == planPrueba.IdProyecto 
                                && p.id_responsable == planPrueba.IdResponsable
                                && p.descripcion == planPrueba.Descripcion
                                select p).FirstOrDefault();
            if (productoEnDb == null)
            {
                return false;
            }

            return true;

        }

        public IList<PlanesDePruebas> BuscarConFiltros(string nombre, int? idResponsable, int? idProyecto)
        {
            var planesDePrueba = (from pl in _bugTrackerContext.PlanesDePrueba
                                  join pr in _bugTrackerContext.Proyectos on pl.id_proyecto equals pr.id_proyecto
                                  join u in _bugTrackerContext.Usuarios on pl.id_responsable equals u.id_usuario into user
                                  from us in user.DefaultIfEmpty()
                                  where pl.borrado == false
                                  && (string.IsNullOrEmpty(pl.nombre) || pl.nombre.ToLower().Contains(nombre))
                                  && (idResponsable == null || pl.id_responsable == idResponsable)
                                  && (idProyecto == null || pl.id_proyecto == idProyecto)
                                  select new PlanesDePruebas
                                  {
                                      IdPlanDePrueba = pl.id_plan_prueba,
                                      Nombre = pl.nombre,
                                      Descripcion = pl.descripcion,
                                      IdResponsable = us.id_usuario,
                                      NombreResponsable = us.usuario,
                                      IdProyecto = pr.id_proyecto,
                                      DescripcionProyecto = pr.descripcion,
                                      Borrado = pl.borrado

                                  }).ToList();
            return planesDePrueba;
        }

        public bool AgregarPlan(PlanesDePruebas planEntidad)
        {
            var planNuevo = new PlanesDePrueba();
            var planPorAñadir = MapperEntidadABD(planEntidad, planNuevo);
            _bugTrackerContext.PlanesDePrueba.Add(planPorAñadir);

            return _bugTrackerContext.SaveChanges() > 0;
        }

        public bool EliminarPlan(int id)
        {
            var PlanEnBd = (from pl in _bugTrackerContext.PlanesDePrueba
                            where pl.id_plan_prueba == id
                            && pl.borrado == false
                            select pl).FirstOrDefault();
            PlanEnBd.borrado = true;

            return _bugTrackerContext.SaveChanges() > 0;


        }
        public bool ActualizarPlan(PlanesDePruebas planEntidad)
        {
            var planEnBd = (from p in _bugTrackerContext.PlanesDePrueba
                            where planEntidad.IdPlanDePrueba == p.id_plan_prueba
                            && p.borrado == false
                            select p).FirstOrDefault();
            MapperEntidadABD(planEntidad, planEnBd);

            return _bugTrackerContext.SaveChanges() > 0;

        }

        public IList<PlanesDePruebas> ObtenerDadosDeBaja()
        {
            var listaPlanes = (from pl in _bugTrackerContext.PlanesDePrueba
                               join pr in _bugTrackerContext.Proyectos on pl.id_proyecto equals pr.id_proyecto
                               join u in _bugTrackerContext.Usuarios on pl.id_responsable equals u.id_usuario into user
                               from us in user.DefaultIfEmpty()
                               where pl.borrado == true
                               select new PlanesDePruebas
                               {
                                   IdPlanDePrueba = pl.id_plan_prueba,
                                   Nombre = pl.nombre,
                                   Descripcion = pl.descripcion,
                                   IdResponsable = us.id_usuario,
                                   NombreResponsable = us.usuario,
                                   IdProyecto = pr.id_proyecto,
                                   DescripcionProyecto = pr.descripcion,
                                   Borrado = pl.borrado

                               }).ToList();
            return listaPlanes;
        }
        public PlanesDePruebas MapperBDAEntidad(PlanesDePrueba planBD, PlanesDePruebas planEntidad)
        {
            if (planBD != null)
            {
                planEntidad.IdPlanDePrueba = planBD.id_proyecto;
                planEntidad.Nombre = planBD.nombre;
                planEntidad.Descripcion = planBD.descripcion;
                planEntidad.Borrado = planBD.borrado;
                planEntidad.IdProyecto = planBD.id_proyecto;
                planEntidad.IdResponsable = planBD.id_responsable;

            }
            else
            {
                return null;
            }

            return planEntidad;
        }
        public PlanesDePrueba MapperEntidadABD(PlanesDePruebas planEntidad, PlanesDePrueba planBD)
        {
            if (planBD != null)
            {
                planBD.id_proyecto = planEntidad.IdPlanDePrueba;
                planBD.nombre = planEntidad.Nombre;
                planBD.descripcion = planEntidad.Descripcion;
                planBD.borrado = planEntidad.Borrado;
                planBD.id_proyecto = planEntidad.IdProyecto;
                planBD.id_responsable = planEntidad.IdResponsable;
            }
            else
            {
                return null;
            }

            return planBD;
        }


    }
}
