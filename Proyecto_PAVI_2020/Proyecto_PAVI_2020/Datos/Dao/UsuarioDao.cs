﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using Proyecto_PAVI_2020.Negocio.Enumeraciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Datos.Dao
{
    public class UsuarioDao
    {
        private readonly BugTrackerEntities _contextBugTracker;

        public UsuarioDao()
        {
            _contextBugTracker = new BugTrackerEntities();
        }
        public Usuario ValidarUsuarioYcontraseña(string nombreUsuario, string contraseña)
        {
            var usuario = new Usuario();
            var usuarioBd = (from u in _contextBugTracker.Usuarios
                             where u.usuario == nombreUsuario && u.password == contraseña
                             && u.borrado == false
                             select u).FirstOrDefault();

            return MapperBDAEntidad(usuarioBd, usuario);
        }

        public bool ActualizarUsuario(Usuario usuario)
        {
            var usuarioEnBD = (from u in _contextBugTracker.Usuarios
                               where usuario.IdUsuario == u.id_usuario
                               select u).FirstOrDefault();

            MapperEntidadABD(usuario, usuarioEnBD);

            return _contextBugTracker.SaveChanges() > 0;
        }

        public IList<Usuario> ObtenerUsuarioTester()
        {
            var listUsuarioEntidad = new List<Usuario>();
            var usuariosTester = (from u in _contextBugTracker.Usuarios
                                  where u.borrado == false && u.id_perfil == (int)PerfilEnum.Tester
                                  select u).ToList();
            foreach (var usuario in usuariosTester)
            {
                var usuarioEntidad = new Usuario();
                MapperBDAEntidad(usuario, usuarioEntidad);
                listUsuarioEntidad.Add(usuarioEntidad);
            }
            return listUsuarioEntidad;
            
        }

        public bool EliminarUsuario(Usuario usuario)
        {
            var usuarioEnBD = (from u in _contextBugTracker.Usuarios
                               where usuario.IdUsuario == u.id_usuario
                               select u).FirstOrDefault();
            usuarioEnBD.borrado = true;

            return _contextBugTracker.SaveChanges() > 0;
        }

        public IList<Usuario> ObtenerUsuarios()
        {
            var listaUsuarios = new List<Usuario>();
            var usuarios = (from u in _contextBugTracker.Usuarios
                            where u.borrado == false
                            select u).ToList();

            foreach (var usuarioEnBd in usuarios)
            {
                var usuario = new Usuario();
                MapperBDAEntidad(usuarioEnBd, usuario);
                listaUsuarios.Add(usuario);
            }
            return listaUsuarios;
        }

        public bool AgregarUsuario(Usuario usuario)
        {
            var usuarioPorAñadir = new Usuarios();
            var nuevoUsuario = MapperEntidadABD(usuario, usuarioPorAñadir);
            _contextBugTracker.Usuarios.Add(nuevoUsuario);
            return _contextBugTracker.SaveChanges() > 0;
        }

        public Usuario BuscarUsuarioPorId(int idUsuario)
        {
            var usuarioPorId = (from u in _contextBugTracker.Usuarios
                                where idUsuario == u.id_usuario
                                select u).FirstOrDefault();
            var usuario = new Usuario();

            return MapperBDAEntidad(usuarioPorId, usuario);
        }

        public Usuario MapperBDAEntidad(Usuarios usuarioEnBd, Usuario usuario)
        {
            if (usuarioEnBd != null)
            {
                usuario.IdUsuario = usuarioEnBd.id_usuario;
                usuario.Contraseña = usuarioEnBd.password;
                usuario.IdPerfil = usuarioEnBd.id_perfil;
                usuario.Estado = usuarioEnBd.estado;
                usuario.NombreUsuario = usuarioEnBd.usuario;
                usuario.Email = usuarioEnBd.email;
                return usuario;
            }
            else
            {
                return null;
            }

        }

        public Usuarios MapperEntidadABD(Usuario usuario, Usuarios usuarioBd)
        {
            usuarioBd.id_usuario = usuario.IdUsuario;
            usuarioBd.password = usuario.Contraseña;
            usuarioBd.id_perfil = usuario.IdPerfil;
            usuarioBd.estado = usuario.Estado;
            usuarioBd.usuario = usuario.NombreUsuario;
            usuarioBd.email = usuario.Email;
            return usuarioBd;
        }
        public bool RecuperarContraseña(string nombreUsuario,string contraseñaNueva)
        {
            var usuario = (from u in _contextBugTracker.Usuarios
                           where u.usuario == nombreUsuario && u.borrado == false
                           select u).FirstOrDefault();
            if (usuario != null)
            {
                usuario.password = contraseñaNueva;      
            }
            return _contextBugTracker.SaveChanges() > 0;


        }

    }
}
