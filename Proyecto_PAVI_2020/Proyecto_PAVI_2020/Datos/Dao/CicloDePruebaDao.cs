﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Datos.Dao
{
    public class CicloDePruebaDao
    {
        private readonly BugTrackerEntities _bugTrackerContext;
        
        public CicloDePruebaDao()
        {
            _bugTrackerContext = new BugTrackerEntities();
        }

        public bool TransaccionAgregarCiclo(CicloDePrueba cicloDePrueba, DetalleCicloDePrueba detalle)
        { 
            using (var dbTransaccion = _bugTrackerContext.Database.BeginTransaction())
            {
                try
                {
                    var detalleCicloDb = new CiclosPruebaDetalle();
                    var cicloPruebaDb = new CiclosPrueba();
                    MapperEntidadABD(cicloDePrueba, cicloPruebaDb, detalle, detalleCicloDb);
                    var ciclo = _bugTrackerContext.CiclosPrueba.Add(cicloPruebaDb);
                    detalleCicloDb.id_ciclo_prueba = ciclo.id_ciclo_prueba;
                    _bugTrackerContext.CiclosPruebaDetalle.Add(detalleCicloDb);
                    _bugTrackerContext.SaveChanges();
                    dbTransaccion.Commit();
                    return true;
                }
                catch (Exception ex)
                {

                    dbTransaccion.Rollback();
                    return false;
                }
            }
                
           

        }

        public bool TransaccionEliminarCiclo(int idCicloDePrueba, int idDetalle)
        {
            using (var dbTransaccion = _bugTrackerContext.Database.BeginTransaction())
            {
                try
                {
                    var cicloPrueba = (from cp in _bugTrackerContext.CiclosPrueba
                                       where cp.id_ciclo_prueba == idCicloDePrueba
                                       && cp.borrado == false
                                       select cp).FirstOrDefault();

                    var detalle = (from dcp in _bugTrackerContext.CiclosPruebaDetalle
                                   where dcp.id_ciclo_prueba_detalle == idDetalle
                                   && dcp.borrado == false
                                   select dcp).FirstOrDefault();

                    if (detalle != null && cicloPrueba != null)
                    {
                        cicloPrueba.borrado = true;
                        detalle.borrado = true;
                    }
                    _bugTrackerContext.SaveChanges();
                    dbTransaccion.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    dbTransaccion.Rollback();
                    return false;
                }
            }
                
        }
        public  DetalleCicloDePrueba BuscarDetallePorIdCiclo(int id)
        {
           
            var detalle = (from cp in _bugTrackerContext.CiclosPruebaDetalle
                              where cp.borrado == false && cp.id_ciclo_prueba == id
                              select new DetalleCicloDePrueba
                              {
                                  IdCicloDePrueba= cp.id_ciclo_prueba,
                                  IdDetalleCicloPrueba = cp.id_ciclo_prueba_detalle,
                                  IdCasoDePrueba = cp.id_caso_prueba,
                                  IdUsuarioTester = cp.id_usuario_tester,
                                  CantidadDeHoras = cp.cantidad_horas,
                                  FechaEjecucion = cp.fecha_ejecucion,
                                  Aceptado = cp.aceptado,
                                  Borrado= cp.borrado
                                  
                              }).FirstOrDefault();
            return detalle;




        }


        public IList<CicloDePrueba> BuscarConFiltros(int? idResponsable, int idPlanSeleccionado)
        {
            var ciclosPrueba = (from c in _bugTrackerContext.CiclosPrueba
                                join u in _bugTrackerContext.Usuarios on c.id_responsable equals u.id_usuario
                                join pp in _bugTrackerContext.PlanesDePrueba on c.id_plan_prueba equals pp.id_plan_prueba
                                where c.id_plan_prueba == idPlanSeleccionado && c.borrado == false
                                && (idResponsable == null || c.id_responsable == idResponsable)
                                select new CicloDePrueba
                                {
                                    IdCicloPrueba = c.id_ciclo_prueba,
                                    FechaInicioEjecucion = c.fecha_inicio_ejecucion,
                                    FechaFinEjecucion = c.fecha_fin_ejecucion,
                                    IdResponsable = u.id_usuario,
                                    NombreResponsable = u.usuario,
                                    IdPlanDePrueba = pp.id_plan_prueba,
                                    NombrePlanPrueba = pp.nombre,
                                    Aceptado = c.aceptado,
                                    Borrado = c.borrado
                                }).ToList();

            return ciclosPrueba;
        }

        public bool TransaccionEditarCiclo(CicloDePrueba cicloEntidad, DetalleCicloDePrueba detalleEntidad)
        {
            using (var dbTransaccion = _bugTrackerContext.Database.BeginTransaction())
            {
                try
                {
                    var cicloPrueba = (from cp in _bugTrackerContext.CiclosPrueba
                                       where cp.id_ciclo_prueba == cicloEntidad.IdCicloPrueba
                                       && cp.borrado == false
                                       select cp).FirstOrDefault();

                    var detalle = (from dcp in _bugTrackerContext.CiclosPruebaDetalle
                                   where dcp.id_ciclo_prueba_detalle == detalleEntidad.IdDetalleCicloPrueba
                                   && dcp.borrado == false
                                   select dcp).FirstOrDefault();

                    MapperEntidadABD(cicloEntidad, cicloPrueba, detalleEntidad, detalle);
                    _bugTrackerContext.SaveChanges();
                    dbTransaccion.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    dbTransaccion.Rollback();
                    return false;
                }
            }
                
            
        }

        public void MapperEntidadABD(CicloDePrueba cicloEntidad, CiclosPrueba ciclosDB, DetalleCicloDePrueba detalleEntidad, CiclosPruebaDetalle detalleBd)
        {

            //Mapper ciclos de prueba
            
            ciclosDB.id_plan_prueba = cicloEntidad.IdPlanDePrueba;
            ciclosDB.id_responsable = cicloEntidad.IdResponsable;
            ciclosDB.fecha_inicio_ejecucion = cicloEntidad.FechaInicioEjecucion;
            ciclosDB.fecha_fin_ejecucion = cicloEntidad.FechaFinEjecucion;
            ciclosDB.aceptado = cicloEntidad.Aceptado;
            ciclosDB.borrado = cicloEntidad.Borrado;

            //Mapper detalle de ciclos de prueba
           
            detalleBd.id_ciclo_prueba = detalleEntidad.IdCicloDePrueba;
            detalleBd.id_caso_prueba = detalleEntidad.IdCasoDePrueba;
            detalleBd.id_usuario_tester = detalleEntidad.IdUsuarioTester;
            detalleBd.fecha_ejecucion = detalleEntidad.FechaEjecucion;
            detalleBd.cantidad_horas = detalleEntidad.CantidadDeHoras;
            detalleBd.aceptado = detalleEntidad.Aceptado;
            detalleBd.borrado = detalleEntidad.Borrado;

        }

        public void MapperBDAEntidad(CiclosPrueba ciclosDB, CicloDePrueba cicloEntidad, DetalleCicloDePrueba detalleEntidad, CiclosPruebaDetalle detalleBd)
        {

            //Mapper ciclos de prueba
            if (cicloEntidad != null)
            {
                cicloEntidad.IdCicloPrueba = ciclosDB.id_ciclo_prueba;
                cicloEntidad.IdPlanDePrueba = ciclosDB.id_plan_prueba;
                cicloEntidad.IdResponsable = ciclosDB.id_responsable;
                cicloEntidad.FechaInicioEjecucion = ciclosDB.fecha_inicio_ejecucion;
                cicloEntidad.FechaFinEjecucion = ciclosDB.fecha_fin_ejecucion;
                cicloEntidad.Aceptado = ciclosDB.aceptado;
                cicloEntidad.Borrado = ciclosDB.borrado;
            }

            //Mapper detalle de ciclos de prueba
            if (detalleEntidad != null)
            {
                detalleEntidad.IdDetalleCicloPrueba = detalleBd.id_ciclo_prueba_detalle;
                detalleEntidad.IdCicloDePrueba = detalleBd.id_ciclo_prueba;
                detalleEntidad.IdCasoDePrueba = detalleBd.id_caso_prueba;
                detalleEntidad.IdUsuarioTester = detalleBd.id_usuario_tester;
                detalleEntidad.FechaEjecucion = detalleBd.fecha_ejecucion;
                detalleEntidad.CantidadDeHoras = detalleBd.cantidad_horas;
                detalleEntidad.Aceptado = detalleBd.aceptado;
                detalleEntidad.Borrado = detalleBd.borrado;
            }
            
        }

        public IList<CicloDePrueba> ObtenerTodos(int idPlanPrueba)
        {
            var ciclosPrueba = (from c in _bugTrackerContext.CiclosPrueba
                                join u in _bugTrackerContext.Usuarios on c.id_responsable equals u.id_usuario
                                join pp in _bugTrackerContext.PlanesDePrueba on c.id_plan_prueba equals pp.id_plan_prueba
                                where c.id_plan_prueba == idPlanPrueba && c.borrado == false
                                select new CicloDePrueba
                                {
                                    IdCicloPrueba = c.id_ciclo_prueba,
                                    FechaInicioEjecucion = c.fecha_inicio_ejecucion,
                                    FechaFinEjecucion = c.fecha_fin_ejecucion,
                                    IdResponsable = u.id_usuario,
                                    NombreResponsable = u.usuario,
                                    IdPlanDePrueba = pp.id_plan_prueba,
                                    NombrePlanPrueba = pp.nombre,
                                    Aceptado = c.aceptado,
                                    Borrado = c.borrado
                                }).ToList();

            return ciclosPrueba;
        }

        public IList<CicloDePrueba> ObtenerDadosBaja(int idPlanPrueba)
        {
            var ciclosPrueba = (from c in _bugTrackerContext.CiclosPrueba
                                join u in _bugTrackerContext.Usuarios on c.id_responsable equals u.id_usuario
                                join pp in _bugTrackerContext.PlanesDePrueba on c.id_plan_prueba equals pp.id_plan_prueba
                                where c.id_plan_prueba == idPlanPrueba && c.borrado == true
                                select new CicloDePrueba
                                {
                                    IdCicloPrueba = c.id_ciclo_prueba,
                                    FechaInicioEjecucion = c.fecha_inicio_ejecucion,
                                    FechaFinEjecucion = c.fecha_fin_ejecucion,
                                    IdResponsable = u.id_usuario,
                                    NombreResponsable = u.usuario,
                                    IdPlanDePrueba = pp.id_plan_prueba,
                                    NombrePlanPrueba = pp.nombre,
                                    Aceptado = c.aceptado,
                                    Borrado = c.borrado
                                }).ToList();

            return ciclosPrueba;
        }
    }
}
