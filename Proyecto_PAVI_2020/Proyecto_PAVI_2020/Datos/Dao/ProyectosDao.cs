﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Datos.Dao
{
    public class ProyectosDao
    {
        private readonly BugTrackerEntities _bugTrackerContext;

        public ProyectosDao()
        {
            _bugTrackerContext = new BugTrackerEntities();
        }
        public IList<Proyecto> BuscarPorFiltro(string descripcion, int? idResponsable, int? idProducto)

        {
            var listaProyectos = (from p in _bugTrackerContext.Proyectos
                                  join pr in _bugTrackerContext.Productos on p.id_producto equals pr.id_producto into productos
                                  join u in _bugTrackerContext.Usuarios on p.id_responsable equals u.id_usuario into user
                                  from us in user.DefaultIfEmpty()
                                  from pro in productos.DefaultIfEmpty()
                                  where p.borrado == false
                                  && (string.IsNullOrEmpty(p.descripcion) || p.descripcion.ToLower().Contains(descripcion.ToLower()))
                                  || ((idProducto == null || p.id_producto == idProducto) && (idResponsable == null || p.id_responsable == idResponsable)) 
                                  && (idProducto == null || p.id_producto == idProducto)
                                  && (idResponsable == null || p.id_responsable == idResponsable)
                                  select new Proyecto
                                  {
                                      IdProyecto = p.id_proyecto,
                                      Descripcion = p.descripcion,
                                      Alcance = p.alcance,
                                      Version = p.version,
                                      IdProducto = pro.id_producto,
                                      NombreProducto = pro.nombre,
                                      IdResponsable = us.id_usuario,
                                      NombreResponsable = us.usuario,
                                      Borrado = p.borrado
                                  }).ToList();



            return listaProyectos;
        }
        

        public Proyecto BuscarPorId(int id)
        {
            var proyectoEntidad = new Proyecto();
            var proyectoBd = (from p in _bugTrackerContext.Proyectos
                            where p.borrado == false && p.id_proyecto == id
                            select p).FirstOrDefault();
            return MapperBdAEntidad(proyectoBd, proyectoEntidad);
        }

        

        public IList<Proyecto> ObtenerProyectos()
        {
             
            var listaProyectos = (from p in _bugTrackerContext.Proyectos
                                 join pr in _bugTrackerContext.Productos on p.id_producto equals pr.id_producto into producto
                                 join u in _bugTrackerContext.Usuarios on p.id_responsable equals u.id_usuario into user
                                 from us in user.DefaultIfEmpty()
                                 from pro in producto.DefaultIfEmpty()
                                 where p.borrado == false
                                 select new Proyecto
                                 { 
                                     IdProyecto = p.id_proyecto,
                                     Descripcion = p.descripcion,
                                     Alcance = p.alcance,
                                     Version = p.version,
                                     IdProducto = pro.id_producto,
                                     NombreProducto = pro.nombre,
                                     IdResponsable= us.id_usuario,
                                     NombreResponsable= us.usuario
                                 }).ToList();

           
        
            return listaProyectos;
        }
        public IList<Proyecto> ObtenerDadosDeBaja()
        {
            var listaProyectos = (from p in _bugTrackerContext.Proyectos
                                  join pr in _bugTrackerContext.Productos on p.id_producto equals pr.id_producto into producto
                                  join u in _bugTrackerContext.Usuarios on p.id_responsable equals u.id_usuario into user
                                  from us in user.DefaultIfEmpty()
                                  from pro in producto.DefaultIfEmpty()
                                  where p.borrado == true
                                  select new Proyecto
                                  {
                                      IdProyecto = p.id_proyecto,
                                      Descripcion = p.descripcion,
                                      Alcance = p.alcance,
                                      Version = p.version,
                                      IdProducto = pro.id_producto,
                                      NombreProducto = pro.nombre,
                                      IdResponsable = us.id_usuario,
                                      NombreResponsable = us.usuario
                                  }).ToList();



            return listaProyectos;
        }

        public bool AgregarProyecto(Proyecto proyectoEntidad)
        {
            var proyectoNuevo = new Proyectos();
            proyectoNuevo.borrado = false;
            var proyectoPorAñadir = MapperEntidadABD(proyectoEntidad, proyectoNuevo);
            _bugTrackerContext.Proyectos.Add(proyectoPorAñadir);

            return _bugTrackerContext.SaveChanges() > 0;
        }

        public bool EliminarProyecto(Proyecto proyectoEntidad)
        {
            var proyectoBd = (from p in _bugTrackerContext.Proyectos
                              where proyectoEntidad.IdProyecto == p.id_proyecto
                              && p.borrado == false
                              select p).FirstOrDefault();
            proyectoBd.borrado = true;

            return _bugTrackerContext.SaveChanges() > 0;
        }

        public bool ActualizarProyecto(Proyecto proyectoEntidad)
        {
            var proyectoBd = (from p in _bugTrackerContext.Proyectos
                              where proyectoEntidad.IdProyecto == p.id_proyecto
                              && p.borrado == false
                              select p).FirstOrDefault();
            MapperEntidadABD(proyectoEntidad, proyectoBd);

            return _bugTrackerContext.SaveChanges() > 0;
        }

        public Proyecto MapperBdAEntidad(Proyectos proyectoBd, Proyecto proyectoEntidad)
        {
            if (proyectoBd != null)
            {

                proyectoEntidad.IdProyecto = proyectoBd.id_proyecto;
                proyectoEntidad.IdProducto = proyectoBd.id_producto;
                
                proyectoEntidad.Descripcion = proyectoBd.descripcion;
                proyectoEntidad.Version = proyectoBd.version;
                proyectoEntidad.Alcance = proyectoBd.alcance;
                proyectoEntidad.IdResponsable = proyectoBd.id_responsable;  
                proyectoEntidad.Borrado = (bool)proyectoBd.borrado;
            }
            else
            {
                return null;
            }

            return proyectoEntidad;
        }


        public Proyectos MapperEntidadABD(Proyecto proyectoEntidad, Proyectos proyectoBd)
        {
            if (proyectoBd != null)
            {

                proyectoBd.id_proyecto = proyectoEntidad.IdProyecto;
                proyectoBd.id_producto = proyectoEntidad.IdProducto;
                proyectoBd.descripcion = proyectoEntidad.Descripcion;
                proyectoBd.version = proyectoEntidad.Version;
                proyectoBd.alcance = proyectoEntidad.Alcance;
                proyectoBd.id_responsable = proyectoEntidad.IdResponsable;
                proyectoBd.borrado = proyectoEntidad.Borrado;
            }
            else
            {
                return null;
            }

            return proyectoBd;
        }
        public bool ExisteProyecto (Proyecto proyecto)
        {
            var proyectoEnBD = (from p in _bugTrackerContext.Proyectos
                                where p.borrado == false 
                                && p.descripcion == proyecto.Descripcion
                                && p.alcance == proyecto.Alcance
                                && p.version == proyecto.Version
                                && p.id_responsable == proyecto.IdResponsable
                                && p.id_producto == proyecto.IdProducto

                                select p).FirstOrDefault();
            if (proyectoEnBD == null)
            {
                return false;
            }

            return true;
        }

    }
}
