﻿using Proyecto_PAVI_2020.Datos.DbContext;
using Proyecto_PAVI_2020.Negocio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_PAVI_2020.Datos.Dao
{
    public class ProductosDao
    {
        private readonly BugTrackerEntities _contextBugTracker;

        public ProductosDao()
        {
            _contextBugTracker = new BugTrackerEntities();
        }

        //creacion del metodo mapper para mappear de entidad a BD
        public Productos MapperEntidadABD(Producto productoEntidad, Productos productosBd)
        {
            productosBd.id_producto = productoEntidad.IdProducto;
            productosBd.nombre = productoEntidad.NombreProducto;
            productosBd.borrado = productoEntidad.Borrado;

            return productosBd;
        }

        //creacion del metodo mapper para mapear desde la BD a la entidad
        public Producto MapperBDAEntidad(Productos productosBd, Producto productoEntidad)
        {
            if (productosBd != null)
            {
                productoEntidad.IdProducto = productosBd.id_producto;
                productoEntidad.NombreProducto = productosBd.nombre;
                productoEntidad.Borrado = (bool)productosBd.borrado;
            }
            else
            {
                return null;
            }
            
            return productoEntidad;
        }

        //metodo para poder actualizar un producto de la base de datos
        public bool ActualizarProductos(Producto productoEntidad)
        {
            //buscamos un producto de la bd por su id que sea igual nuestro id en nuestra entidad y modificamos
            var productoBD = (from p in _contextBugTracker.Productos
                              where productoEntidad.IdProducto == p.id_producto && p.borrado == false
                              select p).FirstOrDefault();

            MapperEntidadABD(productoEntidad, productoBD);

            return _contextBugTracker.SaveChanges() > 0;

        }
        public bool EliminarProductos(Producto productoEntidad)
        {
            //buscamos un producto de la bd por su id que sea igual nuestro id en nuestra entidad y cambiamos el valor de borrado
            var productoBd = (from p in _contextBugTracker.Productos
                              where productoEntidad.IdProducto == p.id_producto && p.borrado == false
                              select p).FirstOrDefault();
            productoBd.borrado = true;

            return _contextBugTracker.SaveChanges() > 0;

        }
        public bool AgregarProductos(Producto productoEntidad)
        {
            var productoNuevo = new Productos();
            productoNuevo.borrado = false;
            var productoPorAñadir = MapperEntidadABD(productoEntidad, productoNuevo);
            _contextBugTracker.Productos.Add(productoPorAñadir);

            return _contextBugTracker.SaveChanges() > 0;

        }
        public IList<Producto> ObtenerProductos()
        {/* se obtiene una lista de objeto de entity framework  de la base y luego se lo transforma 
             en el foreach en objetos entidad de c#  */

            
            var listaProductos = new List<Producto>();
            var productos = (from p in _contextBugTracker.Productos
                             where p.borrado == false
                             orderby p.nombre
                             select p).ToList();

            foreach (var productoBd in productos)
            {
                var productoEntidad = new Producto();
                productoEntidad = MapperBDAEntidad(productoBd, productoEntidad);
                listaProductos.Add(productoEntidad);

            }
            return listaProductos;
        }
        public IList<Producto> ObtenerDadosDeBaja()
        {/* se obtiene una lista de objeto de entity framework  de la base y luego se lo transforma 
             en el foreach en objetos entidad de c#  */


            var listaProductos = new List<Producto>();
            var productos = (from p in _contextBugTracker.Productos
                             orderby p.nombre
                             where p.borrado == true
                             select p).ToList();

            foreach (var productoBd in productos)
            {
                var productoEntidad = new Producto();
                productoEntidad = MapperBDAEntidad(productoBd, productoEntidad);
                listaProductos.Add(productoEntidad);

            }
            return listaProductos;
        }


        public Producto BuscarPorId(int idProducto)
        {
            var productoEnBd = (from p in _contextBugTracker.Productos
                           where idProducto == p.id_producto && p.borrado == false 
                           select p).FirstOrDefault();

            var productoEntidad = new Producto();
            
            return MapperBDAEntidad(productoEnBd, productoEntidad);
        }
        
        public IList<Producto> BuscarPorNombre(string nombre)
        {
            var productos = (from p in _contextBugTracker.Productos
                                where p.nombre.ToLower().Contains(nombre.ToLower())
                                && p.borrado == false
                                select p).ToList();

            var lista = new List<Producto>();
            foreach(var productoEnBd in productos)
            {
                var productoEntidad = new Producto();
                productoEntidad = MapperBDAEntidad(productoEnBd, productoEntidad);
                lista.Add(productoEntidad);

            }
            return lista;   
        }

        public bool ExisteProductoEnBd(string nombre)
        {
            var productoEnDb = (from p in _contextBugTracker.Productos
                            where p.borrado == false && p.nombre == nombre
                            select p).FirstOrDefault();
            if(productoEnDb == null)
            {
                return false;
            }

            return true;
        }
    }

    
}
