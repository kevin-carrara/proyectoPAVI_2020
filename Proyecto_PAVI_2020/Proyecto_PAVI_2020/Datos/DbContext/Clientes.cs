//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Proyecto_PAVI_2020.Datos.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class Clientes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Clientes()
        {
            this.Facturas = new HashSet<Facturas>();
        }
    
        public int id_cliente { get; set; }
        public string cuit { get; set; }
        public string razon_social { get; set; }
        public Nullable<bool> borrado { get; set; }
        public string calle { get; set; }
        public string numero { get; set; }
        public Nullable<System.DateTime> fecha_alta { get; set; }
        public Nullable<int> id_barrio { get; set; }
        public Nullable<int> id_contacto { get; set; }
    
        public virtual Barrios Barrios { get; set; }
        public virtual Contactos Contactos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Facturas> Facturas { get; set; }
    }
}
