//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Proyecto_PAVI_2020.Datos.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ObjetivosCursos
    {
        public int id_objetivo { get; set; }
        public int id_curso { get; set; }
        public Nullable<int> puntos { get; set; }
        public Nullable<bool> borrado { get; set; }
    
        public virtual Cursos Cursos { get; set; }
        public virtual Objetivos Objetivos { get; set; }
    }
}
